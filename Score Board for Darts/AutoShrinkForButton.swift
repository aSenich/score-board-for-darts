//
//  AutoShrinkForButton.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 10/13/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit

class AutoShrinkForButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var adjustTitleFontSizeToWidth: Bool = false; var number: Double = 0.5 {
        didSet {
            self.titleLabel?.adjustsFontSizeToFitWidth = adjustTitleFontSizeToWidth
            self.titleLabel?.minimumScaleFactor = CGFloat(number)
        }
    }

}
