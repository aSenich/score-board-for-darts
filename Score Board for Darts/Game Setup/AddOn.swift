//
//  AddOn.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/21/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class AddOn: UIViewController {
    
    var managedContext: NSManagedObjectContext!
    
    
    
    //    MARK: Outlets
    @IBOutlet weak var adBanner: GADBannerView!
    
    
    // MARK: Actions
    @IBAction func two(_ sender: Any) {
        enterNumberOfPlayers(NumberOfPlayer: 2)
        performSegue(withIdentifier: "toNames", sender: self)
    }
    @IBAction func three(_ sender: Any) {
        enterNumberOfPlayers(NumberOfPlayer: 3)
        performSegue(withIdentifier: "toNames", sender: self)
    }
    @IBAction func four(_ sender: Any) {
        enterNumberOfPlayers(NumberOfPlayer: 4)
        performSegue(withIdentifier: "toNames", sender: self)
    }
    @IBAction func back(_ sender: Any) {
        performSegue(withIdentifier: "toRoot", sender: self)
    }
    

//    Loading
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        //Ad
        self.view.addSubview(adBanner)
        
        
        adBanner.center.x = self.view.center.x
        adBanner.frame.origin.y = self.view.frame.height - adBanner.frame.height
        
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
 
        

        
        

    }
    

    override func viewDidDisappear(_ animated: Bool) {
        self.adBanner.removeFromSuperview()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextvc = segue.destination as! ViewController2
        nextvc.managedContext = managedContext
        
    }
    
    func enterNumberOfPlayers(NumberOfPlayer: Int) {
        let entity = NSEntityDescription.entity(forEntityName: "CurrentGameInfo", in: managedContext)!
        let currentGameInfo = CurrentGameInfo(entity: entity, insertInto: managedContext)
        
        currentGameInfo.currentPlayer = 1
        currentGameInfo.numberOfPlayers = Int16(NumberOfPlayer)
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save CurrentGameInfo data, \(error), \(error.userInfo)")
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
