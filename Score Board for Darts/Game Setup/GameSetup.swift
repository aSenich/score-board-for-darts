//
//  GameSetup.swift
//  Score Board for Darts
//
//  Created by Mark Senich on 1/30/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import UIKit
import CoreData

class GameSetup: UITableViewController, UITextFieldDelegate {
    
    var managedContext: NSManagedObjectContext!
    
    var p1: PlayerInfo!
    var p2: PlayerInfo!
    var p3: PlayerInfo!
    var p4: PlayerInfo!
    
    var gameInfo: CurrentGameInfo!
    
    var NumberOfPlayers = 4
    var Player1Name = ""
    var Player2Name = ""
    var Player3Name = ""
    var Player4Name = ""
    
    

    //    MARK: Outlets
    @IBOutlet weak var segmentedController: UISegmentedControl!
    @IBOutlet weak var player1Text: UILabel!
    @IBOutlet weak var player1TextField: UITextField!
    @IBOutlet weak var player2Text: UILabel!
    @IBOutlet weak var player2TextField: UITextField!
    @IBOutlet weak var player3Text: UILabel!
    @IBOutlet weak var player3TextField: UITextField!
    @IBOutlet weak var player4Text: UILabel!
    @IBOutlet weak var player4TextField: UITextField!
    @IBOutlet weak var beginGameBtn: UIBarButtonItem!
    
    @IBAction func player1TextField(_ sender: Any) {
         Player1Name = player1TextField.text!
    }
    @IBAction func player2TextField(_ sender: Any) {
        Player2Name = player2TextField.text!
    }
    @IBAction func player3TextField(_ sender: Any) {
        Player3Name = player3TextField.text!
    }
    @IBAction func player4TextField(_ sender: Any) {
        Player4Name = player4TextField.text!
    }
    
    

//    Actions
    @IBAction func segmentedController(_ sender: Any) {
        let index = segmentedController.selectedSegmentIndex
        
        switch index {
        case 0:
            NumberOfPlayers = 2
            player3Text.isHidden = true
            player3TextField.isHidden = true
            player4Text.isHidden = true
            player4TextField.isHidden = true
        case 1:
            NumberOfPlayers = 3
            player3Text.isHidden = false
            player3TextField.isHidden = false
            player4Text.isHidden = true
            player4TextField.isHidden = true
        case 2:
            NumberOfPlayers = 4
            player3Text.isHidden = false
            player3TextField.isHidden = false
            player4Text.isHidden = false
            player4TextField.isHidden = false
            
        default:
            break
        }
        
        
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func beginGame(_ sender: Any) {
        enterNumberOfPlayers(NumberOfPlayer: NumberOfPlayers)
        fetchGameInfo()
        switch (gameInfo.numberOfPlayers) {
        case 2:
            enterPlayer1Name(name: Player1Name)
            enterPlayer2Name(name: Player2Name)
            enterPlayer3Name(name: "No Player3")
            enterPlayer4Name(name: "No Player4")
        case 3:
            enterPlayer1Name(name: Player1Name)
            enterPlayer2Name(name: Player2Name)
            enterPlayer3Name(name: Player3Name)
            enterPlayer4Name(name: "No Player4")
        case 4:
            enterPlayer1Name(name: Player1Name)
            enterPlayer2Name(name: Player2Name)
            enterPlayer3Name(name: Player3Name)
            enterPlayer4Name(name: Player4Name)
        default:
            break
        }
        performSegue(withIdentifier: "beginTheGame", sender: self)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        print("Amount in array\(gameresults.count)")

        beginGameBtn.isEnabled = false
        // Do any additional setup after loading the view.
        player1TextField.delegate = self
        player2TextField.delegate = self
        player3TextField.delegate = self
        player4TextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(enableBeginButton), name: NSNotification.Name(rawValue: "UIKeyboardDidHideNotification"), object: nil)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextVc = segue.destination as! ViewController3
        nextVc.managedContext = managedContext
    }
    

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        player1TextField.resignFirstResponder()
        player2TextField.resignFirstResponder()
        player3TextField.resignFirstResponder()
        player4TextField.resignFirstResponder()
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func fetchGameInfo() {
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        
        gameInfo = gameresults.first!
        
    }
    func fetchAll() {
        let request: NSFetchRequest<PlayerInfo> = PlayerInfo.fetchRequest()
        let results = try! managedContext.fetch(request)
        
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        
        gameInfo = gameresults.first!
        /*
         print(results.count)
         switch gameInfo.numberOfPlayers {
         case 2:
         p1 = results[0]
         p2 = results[1]
         case 3:
         p1 = results[0]
         p2 = results[1]
         p3 = results[2]
         case 4:
         p1 = results[0]
         p2 = results[1]
         p3 = results[2]
         p4 = results[3]
         default:
         break
         }
         */
        p1 = results[0]
        p2 = results[1]
        p3 = results[2]
        p4 = results[3]
        
        
    }
    
    func saveData() {
        do {
            try managedContext.save()
        } catch {
            print("Could Not Save Data")
        }
    }
    func enterPlayer1Name(name: String) {
        let entity = NSEntityDescription.entity(forEntityName: "PlayerInfo", in: managedContext)!
        let playerInfo = PlayerInfo(entity: entity, insertInto: managedContext)
        
        playerInfo.bull = 0
        playerInfo.bullClosed = false
        playerInfo.eighteen = 0
        playerInfo.eighteenClosed = false
        playerInfo.fifteen = 0
        playerInfo.fifteenClosed = false
        playerInfo.isUp = true
        playerInfo.name = name
        playerInfo.nineteen = 0
        playerInfo.nineteenClosed = false
        playerInfo.number = 1
        playerInfo.seventeen = 0
        playerInfo.sixteenClosed = false
        playerInfo.sixteen = 0
        playerInfo.sixteenClosed = false
        playerInfo.twenety = 0
        playerInfo.twentyClosed = false
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save player1 data \(error), \(error.userInfo)")
        }
        
        
    }
    
    func enterPlayer2Name(name: String) {
        let entity = NSEntityDescription.entity(forEntityName: "PlayerInfo", in: managedContext)!
        let playerInfo = PlayerInfo(entity: entity, insertInto: managedContext)
        
        playerInfo.bull = 0
        playerInfo.bullClosed = false
        playerInfo.eighteen = 0
        playerInfo.eighteenClosed = false
        playerInfo.fifteen = 0
        playerInfo.fifteenClosed = false
        playerInfo.isUp = false
        playerInfo.name = name
        playerInfo.nineteen = 0
        playerInfo.nineteenClosed = false
        playerInfo.number = 2
        playerInfo.seventeen = 0
        playerInfo.seventeenClosed = false
        playerInfo.sixteen = 0
        playerInfo.sixteenClosed = false
        playerInfo.twenety = 0
        playerInfo.twentyClosed = false
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save player2 data \(error), \(error.userInfo)")
        }
    }
    func enterPlayer3Name(name: String) {
        let entity = NSEntityDescription.entity(forEntityName: "PlayerInfo", in: managedContext)!
        let playerInfo = PlayerInfo(entity: entity, insertInto: managedContext)
        
        playerInfo.bull = 0
        playerInfo.bullClosed = false
        playerInfo.eighteen = 0
        playerInfo.eighteenClosed = false
        playerInfo.fifteen = 0
        playerInfo.fifteenClosed = false
        playerInfo.isUp = false
        playerInfo.name = name
        playerInfo.nineteen = 0
        playerInfo.nineteenClosed = false
        playerInfo.number = 3
        playerInfo.seventeen = 0
        playerInfo.seventeenClosed = false
        playerInfo.sixteen = 0
        playerInfo.sixteenClosed = false
        playerInfo.twenety = 0
        playerInfo.twentyClosed = false
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save player3 data \(error), \(error.userInfo)")
        }
        
    }
    func enterPlayer4Name(name: String) {
        let entity = NSEntityDescription.entity(forEntityName: "PlayerInfo", in: managedContext)!
        let playerInfo = PlayerInfo(entity: entity, insertInto: managedContext)
        
        playerInfo.bull = 0
        playerInfo.bullClosed = false
        playerInfo.eighteen = 0
        playerInfo.eighteenClosed = false
        playerInfo.fifteen = 0
        playerInfo.fifteenClosed = false
        playerInfo.isUp = false
        playerInfo.name = name
        playerInfo.nineteen = 0
        playerInfo.nineteenClosed = false
        playerInfo.number = 4
        playerInfo.seventeen = 0
        playerInfo.seventeenClosed = false
        playerInfo.sixteen = 0
        playerInfo.sixteenClosed = false
        playerInfo.twenety = 0
        playerInfo.twentyClosed = false
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save player4 data \(error), \(error.userInfo)")
        }
        
    }
    
    func enterNumberOfPlayers(NumberOfPlayer: Int) {
        let entity = NSEntityDescription.entity(forEntityName: "CurrentGameInfo", in: managedContext)!
        let currentGameInfo = CurrentGameInfo(entity: entity, insertInto: managedContext)
        
        currentGameInfo.currentPlayer = 1
        currentGameInfo.numberOfPlayers = Int16(NumberOfPlayer)
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save CurrentGameInfo data, \(error), \(error.userInfo)")
        }
    }
    
    @objc func enableBeginButton() {
        switch NumberOfPlayers {
        case 2:
            if player1TextField.text?.isEmpty != true && player2TextField.text?.isEmpty != true {
                beginGameBtn.isEnabled = true
            } else {
                beginGameBtn.isEnabled = false
            }
        case 3:
            if player1TextField.text?.isEmpty != true && player2TextField.text?.isEmpty != true && player3TextField.text != nil {
                beginGameBtn.isEnabled = true
            } else {
                beginGameBtn.isEnabled = false
            }
        case 4:
            if player1TextField.text?.isEmpty != true && player2TextField.text?.isEmpty != true && player3TextField.text?.isEmpty != true && player4TextField.text?.isEmpty != true{
                beginGameBtn.isEnabled = true
            } else {
                beginGameBtn.isEnabled = false
            }
        default:
            break
        }
    }

}
