//
//  ViewController.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/8/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class ViewController: UIViewController {

    var managedContext: NSManagedObjectContext!
    var gameInfo: CurrentGameInfo!
    
    
    @IBAction func unwindToRoot(segue:UIStoryboardSegue) {}
    
    
    // MARK: Outlets
    @IBOutlet weak var welcomeSign: UILabel!
    @IBOutlet weak var darts: UILabel!
    @IBOutlet weak var dartBoard: UIImageView!
    @IBOutlet weak var scoreBoard: UILabel!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var settingsSoonText: UILabel!
    @IBOutlet weak var howToPlayBtn: UIButton!
    @IBOutlet weak var directionSoonText: UILabel!
    @IBOutlet weak var adBanner: GADBannerView!
    
    // MARK: Actions
    @IBAction func howToPlayBtn(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            self.directionSoonText.alpha = 1.0
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(withDuration: 1, animations: {
                self.directionSoonText.alpha = 0.0
            })
        }
    }
    @IBAction func settingsBtn(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            self.settingsSoonText.alpha = 1.0
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(withDuration: 1, animations: {
                self.settingsSoonText.alpha = 0.0
            })
        }
        
    }
    
    @IBAction func StartBtn(_ sender: Any) {
        performSegue(withIdentifier: "toGameSetUp", sender: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        settingsBtn.isHidden = true
//        howToPlayBtn.isHidden = true
//        startBtn.isHidden = true
        

        print("X: \(welcomeSign.center)")
        
        
        


        /* adBanner = GADBannerView(adSize: kGADAdSizeSmartBannerLandscape)
 */
        
        self.view.addSubview(adBanner)
        //    ?????
//        adBanner = GADBannerView(adSize: kGADAdSizeSmartBannerLandscape)
        //      ?????


        adBanner.center.x = self.view.center.x
        adBanner.frame.origin.y = self.view.frame.height - adBanner.frame.height
        
        // Do any additional setup after loading the view, typically from a nib.
    

        // Sets up coming soon texts
        settingsSoonText.alpha = 0.0
        directionSoonText.alpha = 0.0
 
        
        //Ad
       
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        fetch()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.adBanner.removeFromSuperview()
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ResumeSegue"{
            let nextVC = segue.destination as! ViewController3
            nextVC.managedContext = managedContext
            nextVC.rusumingGame = true
        } else {
        let nextvc = segue.destination as! GameSetup
        nextvc.managedContext = managedContext
        }
    }

    func fetch() {
        let request: NSFetchRequest<PlayerInfo> = PlayerInfo.fetchRequest()
        let results = try! managedContext.fetch(request)
        
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        
        
        if Int(results.count) > 0 {
            presentContinueGameAlert()
        } 
        
    }
    
    func presentContinueGameAlert() {
        let alert = UIAlertController(title: "Continue Game?", message: "It looks like you didn't finish your last game. Would you like to resume?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "New Game", style: UIAlertAction.Style.destructive, handler: {action in
            //       Clears all player info
            let PlayerInfoFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "PlayerInfo")
            
            let deleteAllPlayerInfo = NSBatchDeleteRequest(fetchRequest: PlayerInfoFetch)
            
            do {
                let PlayerInfoDeleteResults = try self.managedContext.execute(deleteAllPlayerInfo)
            } catch {
                print("Something went wrong when attempting to delete all player info")
            }
            
            //        Clears all game info
            let GameInfoFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CurrentGameInfo")
            
            let deleteAllCurentGameInfo = NSBatchDeleteRequest(fetchRequest: GameInfoFetchRequest)
            
            do {
                let CurrentGameInfoDeleteResults = try self.managedContext.execute(deleteAllCurentGameInfo)
            } catch {
                print("There was an error when attempting to delete all Current Game data")
            }
            self.performSegue(withIdentifier: "toGameSetUp", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "Resume", style: UIAlertAction.Style.default, handler: {action in
            self.performSegue(withIdentifier: "ResumeSegue", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
    }
 
    
    
}
