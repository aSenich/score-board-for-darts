//
//  ViewController2.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/9/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class ViewController2: UIViewController, UITextFieldDelegate {
    
    var managedContext: NSManagedObjectContext!

    var gameInfo: CurrentGameInfo!
    

    
    

    // MARK: Actions


    
    
    // MARK: Outlets
    @IBOutlet weak var playerNamesTitle: UILabel!
    @IBOutlet weak var player1Name: UITextField!
    @IBOutlet weak var player2Name: UITextField!
    @IBOutlet weak var player3Name: UITextField!
    @IBOutlet weak var player4Name: UITextField!
    @IBOutlet weak var buttonQ: UILabel!
    @IBOutlet weak var boxesDirections: UILabel!
    @IBOutlet weak var continueBtnDirections: UILabel!
    @IBOutlet weak var backBtnDirections: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var adBanner: GADBannerView!
    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    @IBOutlet weak var activityIndText: UILabel!
    @IBOutlet weak var questionBtn: UIButton!
    
    // MARK: Actions
    @IBAction func `continue`(_ sender: Any) {
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func Question(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
        self.buttonQ.alpha = 0.0
        self.boxesDirections.alpha = 1.0
        self.continueBtnDirections.alpha = 1.0
        self.backBtnDirections.alpha = 1.0
        })
        UIView.animate(withDuration: 1, animations: {
        DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
            self.buttonQ.alpha = 1.0
            self.boxesDirections.alpha = 0.0
            self.continueBtnDirections.alpha = 0.0
            self.backBtnDirections.alpha = 0.0
        }
        })
    }
    
    @IBAction func continueBtn(_ sender: Any) {
        
        setMissingPlayerNames()
        
        switch gameInfo.numberOfPlayers {
        case 2:
            if player1Name.text != nil || player2Name.text != nil {
                performSegue(withIdentifier: "start", sender: self)
                
            } else {
                unknownPlayer()
            }
        case 3:
            if player1Name.text == nil || player2Name.text == nil || player3Name.text == nil {
                
                unknownPlayer()
            } else {
                performSegue(withIdentifier: "start", sender: self)
            }
        case 4:
            if player1Name.text == nil || player2Name.text == nil || player3Name.text == nil || player4Name.text == nil {
                
                unknownPlayer()
            } else {
                performSegue(withIdentifier: "start", sender: self)
            }
        default:
            print("Error with number of players")
        }

    }
    
//    Loading
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetch()
        
        print(gameInfo.numberOfPlayers)
        questionBtn.isHidden = true
        
        buttonQ.isHidden = true
        
        //Ad
        self.view.addSubview(adBanner)
        
        
        adBanner.center.x = self.view.center.x
        adBanner.frame.origin.y = self.view.frame.height - adBanner.frame.height
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())


        boxesDirections.alpha = 0.0
        continueBtnDirections.alpha = 0.0
        backBtnDirections.alpha = 0.0
        buttonQ.alpha = 0.0
        
        activityInd.isHidden = true
        activityIndText.isHidden = true
        
        activityInd.stopAnimating()
        
        switch gameInfo.numberOfPlayers {
        case 2:
            player1Name.isHidden = false
            player2Name.isHidden = false
            player3Name.isHidden = true
            player4Name.isHidden = true
        case 3:
            player1Name.isHidden = false
            player2Name.isHidden = false
            player3Name.isHidden = false
            player4Name.isHidden = true
        case 4:
            player1Name.isHidden = false
            player2Name.isHidden = false
            player3Name.isHidden = false
            player4Name.isHidden = false
        default:
            print("Error in Number of Players tracker")
        }
        // Do any additional setup after loading the view.
        player1Name.delegate=self
        player2Name.delegate=self
        player3Name.delegate=self
        player4Name.delegate=self
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1, animations: {
            self.buttonQ.alpha = 1.0
            })

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide keyboard.
        player1Name.resignFirstResponder()
        player2Name.resignFirstResponder()
        player3Name.resignFirstResponder()
        player4Name.resignFirstResponder()
        return true
    }
//    Text Field Action Test
    @IBAction func p1TextField(_ sender: Any) {
        enterPlayer1Name(name: player1Name.text!)
    }
    @IBAction func p2TextField(_ sender: Any) {
        enterPlayer2Name(name: player2Name.text!)
    }
    @IBAction func p3TextField(_ sender: Any) {
        enterPlayer3Name(name: player3Name.text!)
    }
    @IBAction func p4TextField(_ sender: Any) {
        enterPlayer4Name(name: player4Name.text!)
    }

    
    
    func textFieldDidEndEditing(_ textField: UITextField) {

        /*
        enterPlayer1Name(name: player1Name.text!)
        enterPlayer2Name(name: player2Name.text!)
        enterPlayer3Name(name: player3Name.text!)
        enterPlayer4Name(name: player4Name.text!)
 */
        


    }


    override func viewDidDisappear(_ animated: Bool) {
        
        self.adBanner.removeFromSuperview()
    }
    


    func unknownPlayer () {
        let missingPlayer = UIAlertController(title: "Unknown Player(s) Name", message: "You must enter the name of all players to continue", preferredStyle: UIAlertController.Style.alert)
        
        missingPlayer.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in }))
        
        self.present(missingPlayer, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextVc = segue.destination as! ViewController3
        nextVc.managedContext = managedContext
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    func enterPlayer1Name(name: String) {
        let entity = NSEntityDescription.entity(forEntityName: "PlayerInfo", in: managedContext)!
        let playerInfo = PlayerInfo(entity: entity, insertInto: managedContext)
        
        playerInfo.bull = 0
        playerInfo.bullClosed = false
        playerInfo.eighteen = 0
        playerInfo.eighteenClosed = false
        playerInfo.fifteen = 0
        playerInfo.fifteenClosed = false
        playerInfo.isUp = true
        playerInfo.name = name
        playerInfo.nineteen = 0
        playerInfo.nineteenClosed = false
        playerInfo.number = 1
        playerInfo.seventeen = 0
        playerInfo.sixteenClosed = false
        playerInfo.sixteen = 0
        playerInfo.sixteenClosed = false
        playerInfo.twenety = 0
        playerInfo.twentyClosed = false
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save player1 data \(error), \(error.userInfo)")
        }
        
        
    }
    
    func enterPlayer2Name(name: String) {
        let entity = NSEntityDescription.entity(forEntityName: "PlayerInfo", in: managedContext)!
        let playerInfo = PlayerInfo(entity: entity, insertInto: managedContext)
        
        playerInfo.bull = 0
        playerInfo.bullClosed = false
        playerInfo.eighteen = 0
        playerInfo.eighteenClosed = false
        playerInfo.fifteen = 0
        playerInfo.fifteenClosed = false
        playerInfo.isUp = false
        playerInfo.name = name
        playerInfo.nineteen = 0
        playerInfo.nineteenClosed = false
        playerInfo.number = 2
        playerInfo.seventeen = 0
        playerInfo.seventeenClosed = false
        playerInfo.sixteen = 0
        playerInfo.sixteenClosed = false
        playerInfo.twenety = 0
        playerInfo.twentyClosed = false
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save player2 data \(error), \(error.userInfo)")
        }
    }
    func enterPlayer3Name(name: String) {
        let entity = NSEntityDescription.entity(forEntityName: "PlayerInfo", in: managedContext)!
        let playerInfo = PlayerInfo(entity: entity, insertInto: managedContext)
        
        playerInfo.bull = 0
        playerInfo.bullClosed = false
        playerInfo.eighteen = 0
        playerInfo.eighteenClosed = false
        playerInfo.fifteen = 0
        playerInfo.fifteenClosed = false
        playerInfo.isUp = false
        playerInfo.name = name
        playerInfo.nineteen = 0
        playerInfo.nineteenClosed = false
        playerInfo.number = 3
        playerInfo.seventeen = 0
        playerInfo.seventeenClosed = false
        playerInfo.sixteen = 0
        playerInfo.sixteenClosed = false
        playerInfo.twenety = 0
        playerInfo.twentyClosed = false
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save player3 data \(error), \(error.userInfo)")
        }

    }
    func enterPlayer4Name(name: String) {
        let entity = NSEntityDescription.entity(forEntityName: "PlayerInfo", in: managedContext)!
        let playerInfo = PlayerInfo(entity: entity, insertInto: managedContext)
        
        playerInfo.bull = 0
        playerInfo.bullClosed = false
        playerInfo.eighteen = 0
        playerInfo.eighteenClosed = false
        playerInfo.fifteen = 0
        playerInfo.fifteenClosed = false
        playerInfo.isUp = false
        playerInfo.name = name
        playerInfo.nineteen = 0
        playerInfo.nineteenClosed = false
        playerInfo.number = 4
        playerInfo.seventeen = 0
        playerInfo.seventeenClosed = false
        playerInfo.sixteen = 0
        playerInfo.sixteenClosed = false
        playerInfo.twenety = 0
        playerInfo.twentyClosed = false
        
        do {
            try managedContext.save()
        } catch let error as NSError{
            print("Could not save player4 data \(error), \(error.userInfo)")
        }

    }
    
    func fetch() {
        let request: NSFetchRequest<PlayerInfo> = PlayerInfo.fetchRequest()
        let results = try! managedContext.fetch(request)
        
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        
        gameInfo = gameresults.first!
        
        if Int(results.count) > 0 {
            print("Game Not finished")
        } else {
            print("No current game")
        }
        
    }
    
    func setMissingPlayerNames() {
        switch Int(gameInfo.numberOfPlayers) {
        case 2:
            enterPlayer3Name(name: "No Player3")
            enterPlayer4Name(name: "No Player4")
        case 3:
            enterPlayer4Name(name: "No Player4")
            
        default:
            print("Here is the problem")
        }
    }
    
    
    
}
