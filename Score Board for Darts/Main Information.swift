//
//  Main Information.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/21/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit

/*
// Keeps track of the current player.
var currentPlayer = 1


// Lets the game know how many people are playing the game.
var numberOfPlayers = 0


// Keeps track of players names.

var names = [String: String]()

// Keeps track of player1's score.

var p1twenty = 0
var p1twentyClosed = false

var p1nineteen = 0
var p1nineteenClosed = false

var p1eightteen = 0
var p1eightteenClosed = false

var p1seventeen = 0
var p1seventeenClosed = false

var p1sixteen = 0
var p1sixteenClosed = false

var p1fifteen = 0
var p1fifteenClosed = false

var p1bull = 0
var p1bullClosed = false


// Keeps track of player2's score.

var p2twenty = 0
var p2twentyClosed = false

var p2nineteen = 0
var p2nineteenClosed = false

var p2eightteen = 0
var p2eightteenClosed = false

var p2seventeen = 0
var p2seventeenClosed = false

var p2sixteen = 0
var p2sixteenClosed = false

var p2fifteen = 0
var p2fifteenClosed = false

var p2bull = 0
var p2bullClosed = false


// Keeps track of player3's score.

var p3twenty = 0
var p3twentyClosed = false

var p3nineteen = 0
var p3nineteenClosed = false

var p3eightteen = 0
var p3eightteenClosed = false

var p3seventeen = 0
var p3seventeenClosed = false

var p3sixteen = 0
var p3sixteenClosed = false

var p3fifteen = 0
var p3fifteenClosed = false

var p3bull = 0
var p3bullClosed = false


// Keeps track of player4's score.

var p4twenty = 0
var p4twentyClosed = false

var p4nineteen = 0
var p4nineteenClosed = false

var p4eightteen = 0
var p4eightteenClosed = false

var p4seventeen = 0
var p4seventeenClosed = false

var p4sixteen = 0
var p4sixteenClosed = false

var p4fifteen = 0
var p4fifteenClosed = false

var p4bull = 0
var p4bullClosed = false


// Keeps track of data to go back and undo changes when back button is hit from router.

var fromNumber = 0
var hitValue = 0


// Sets a winning player name.

var winningPlayer = ""


// Players total scores

var p1total = 0
var p2total = 0
var p3total = 0
var p4total = 0



// Gives winning view information

var fromWinnerView = false



// From Names view controller

var fromNames = false



var firstTimeLoadingBoard = false




// Tells the app if it is the first run through the app
var firstTime = true


// Tells the app if new view controller was set

var newVCset = false

// Tracks the number of numbers each player selects in their turn

var numberOfNumbersSelectected = 0


// Tells other view the player has changed

var newPlayer = false


// Tracks the history of each player

var p1HisLarge = [String: Int]()
var p1HisSmall = [String: Int]()

var p2HisLarge = [String: Int]()
var p2HisSmall = [String: Int]()

var p3HisLarge = [String: Int]()
var p3HisSmall = [String: Int]()

var p4HisLarge = [String: Int]()
var p4HisSmall = [String: Int]()

// Tracks the nuber of turns each player has had

var p1turns = 0
var p2turns = 0
var p3turns = 0
var p4turns = 0


// Asks player how many numbers they hit

var numberofNumbershit = 0

// Tracks the amount of numbers a player has already selected

var numsSelected = 0
 */
