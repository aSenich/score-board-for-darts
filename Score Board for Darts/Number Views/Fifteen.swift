//
//  Fifteen.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/25/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class Fifteen: UIViewController {
    
    var managedContext: NSManagedObjectContext!
    
    var p1: PlayerInfo!
    var p2: PlayerInfo!
    var p3: PlayerInfo!
    var p4: PlayerInfo!
    
    var gameInfo: CurrentGameInfo!
    
    @IBOutlet weak var adBanner: GADBannerView!
    // MARK: Tesing Outlets
    
    @IBOutlet weak var extraSingle: UIButton!
    @IBOutlet weak var extraSingleLine: UIImageView!
    @IBOutlet weak var line1: UIImageView!
    @IBOutlet weak var line2: UIImageView!
    @IBOutlet weak var line3: UIImageView!
    
    
    
    
    
    // MARK: Action
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func single(_ sender: Any) {
        
        
        gameInfo.fromNumber = 15
        
        gameInfo.hitValue = 1
        
        gameInfo.numberOfNumsSelected = gameInfo.numberOfNumsSelected+1
        saveData()
        fetch()
        switch gameInfo.currentPlayer {
        case 1:
            if p1.fifteenClosed == true {
                if p2.fifteen == 3 || p3.fifteen == 3 || p4.fifteen == 3 {
                   whatToDo()
                } else {
                    p1.fifteen = p1.fifteen+1
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
            p1.fifteen = p1.fifteen+1
                saveData()
                fetch()
            whatToDo()
            }
        case 2:
            if p2.fifteenClosed == true {
                if p1.fifteen == 3 || p3.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p2.fifteen = p2.fifteen+1
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
            p2.fifteen = p2.fifteen+1
                saveData()
                fetch()
            whatToDo()
            }
        case 3:
            if p3.fifteenClosed == true {
                if p1.fifteen == 3 || p2.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p3.fifteen = p3.fifteen+1
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
            p3.fifteen = p3.fifteen+1
                saveData()
                fetch()
            whatToDo()
            }
        case 4:
            if p4.fifteenClosed == true {
                if p1.fifteen == 3 || p2.fifteen == 3 || p3.fifteen == 3 {
                    whatToDo()
                } else {
                    p4.fifteen = p4.fifteen+1
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
            p4.fifteen = p4.fifteen+1
                saveData()
                fetch()
            whatToDo()
            }
        default:
            print("Error with palyer order tracker")
        }
    }
    @IBAction func single2(_ sender: Any) {
        
        gameInfo.fromNumber = 15
        
        gameInfo.hitValue = 1
        
        gameInfo.numberOfNumsSelected = gameInfo.numberOfNumsSelected+1
        saveData()
        fetch()
        switch gameInfo.currentPlayer {
        case 1:
            if p1.fifteenClosed == true {
                if p2.fifteen == 3 || p3.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p1.fifteen = p1.fifteen+1
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p1.fifteen = p1.fifteen+1
                saveData()
                fetch()
                whatToDo()
            }
        case 2:
            if p2.fifteenClosed == true {
                if p1.fifteen == 3 || p3.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p2.fifteen = p2.fifteen+1
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p2.fifteen = p2.fifteen+1
                saveData()
                fetch()
                whatToDo()
            }
        case 3:
            if p3.fifteenClosed == true {
                if p1.fifteen == 3 || p2.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p3.fifteen = p3.fifteen+1
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p3.fifteen = p3.fifteen+1
                saveData()
                fetch()
                whatToDo()
            }
        case 4:
            if p4.fifteenClosed == true {
                if p1.fifteen == 3 || p2.fifteen == 3 || p3.fifteen == 3 {
                    whatToDo()
                } else {
                    p4.fifteen = p4.fifteen+1
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p4.fifteen = p4.fifteen+1
                saveData()
                fetch()
                whatToDo()
            }
        default:
            print("Error with palyer order tracker")
        }
    }
    @IBAction func triple(_ sender: Any) {
        
        gameInfo.fromNumber = 15
        
        gameInfo.hitValue = 3
        
        gameInfo.numberOfNumsSelected = gameInfo.numberOfNumsSelected+1
        saveData()
        fetch()
        switch gameInfo.currentPlayer {
        case 1:
            if p1.fifteenClosed == true {
                if p2.fifteen == 3 || p3.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p1.fifteen = p1.fifteen+3
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p1.fifteen = p1.fifteen+3
                saveData()
                fetch()
                whatToDo()
            }
        case 2:
            if p2.fifteenClosed == true {
                if p1.fifteen == 3 || p3.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p2.fifteen = p2.fifteen+3
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p2.fifteen = p2.fifteen+3
                saveData()
                fetch()
                whatToDo()
            }
        case 3:
            if p3.fifteenClosed == true {
                if p1.fifteen == 3 || p2.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p3.fifteen = p3.fifteen+3
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p3.fifteen = p3.fifteen+3
                saveData()
                fetch()
                whatToDo()
            }
        case 4:
            if p4.fifteenClosed == true {
                if p1.fifteen == 3 || p2.fifteen == 3 || p3.fifteen == 3 {
                    whatToDo()
                } else {
                    p4.fifteen = p4.fifteen+3
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p4.fifteen = p4.fifteen+3
                saveData()
                fetch()
                whatToDo()
            }
        default:
            print("Error with palyer order tracker")
        }
    }
    @IBAction func double(_ sender: Any) {
        
        gameInfo.fromNumber = 15
        
        gameInfo.hitValue = 2
        
        gameInfo.numberOfNumsSelected = gameInfo.numberOfNumsSelected+1
        saveData()
        fetch()
        switch gameInfo.currentPlayer {
        case 1:
            if p1.fifteenClosed == true {
                if p2.fifteen == 3 || p3.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p1.fifteen = p1.fifteen+2
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p1.fifteen = p1.fifteen+2
                saveData()
                fetch()
                whatToDo()
            }
        case 2:
            if p2.fifteenClosed == true {
                if p1.fifteen == 3 || p3.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p2.fifteen = p2.fifteen+2
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p2.fifteen = p2.fifteen+2
                saveData()
                fetch()
                whatToDo()
            }
        case 3:
            if p3.fifteenClosed == true {
                if p1.fifteen == 3 || p2.fifteen == 3 || p4.fifteen == 3 {
                    whatToDo()
                } else {
                    p3.fifteen = p3.fifteen+2
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p3.fifteen = p3.fifteen+2
                saveData()
                fetch()
                whatToDo()
            }
        case 4:
            if p4.fifteenClosed == true {
                if p1.fifteen == 3 || p2.fifteen == 3 || p3.fifteen == 3 {
                    whatToDo()
                } else {
                    p4.fifteen = p4.fifteen+2
                    saveData()
                    fetch()
                    whatToDo()
                }
            } else {
                p4.fifteen = p4.fifteen+2
                saveData()
                fetch()
                whatToDo()
            }
        default:
            print("Error with palyer order tracker")
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        gameInfo.currentLocation = 10
        saveData()
        fetch()
        
        //Ad
        /*
        self.view.addSubview(adBanner)
        
        
        adBanner.center.x = self.view.center.x
        adBanner.frame.origin.y = self.view.frame.height - adBanner.frame.height
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
 */

        // Do any additional setup after loading the view.
      //  lastVC.dismiss(animated: false, completion: nil)
        
        bringAllLinesToFront()
        
        extraSingle.isHidden = true
        extraSingleLine.isHidden = true
    }
    

    func whatToDo() {
        if gameInfo.numberOfNumsSelected != gameInfo.numberOfNumsHit {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPop"), object: nil)
            performSegue(withIdentifier: "15toDart", sender: self)
        } else {
            performSegue(withIdentifier: "15toscores", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "15toscores" {
            let scoreboard = segue.destination as! ScoreBoard
            
            scoreboard.managedContext = managedContext
        }
    }
    
    func bringAllLinesToFront() {
        self.view.bringSubviewToFront(line1)
        self.view.bringSubviewToFront(line2)
        self.view.bringSubviewToFront(line3)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.adBanner.removeFromSuperview()
    }
    
    //    MARK: Core Data Functions
    func fetch() {
        let request: NSFetchRequest<PlayerInfo> = PlayerInfo.fetchRequest()
        let results = try! managedContext.fetch(request)
        
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        
        gameInfo = gameresults.first!
        /*
        switch gameInfo.numberOfPlayers {
        case 2:
            p1 = results[0]
            p2 = results[1]
        case 3:
            p1 = results[0]
            p2 = results[1]
            p3 = results[2]
        case 4:
            p1 = results[0]
            p2 = results[1]
            p3 = results[2]
            p4 = results[3]
        default:
            break
        }
 */
        p1 = results[0]
        p2 = results[1]
        p3 = results[2]
        p4 = results[3]
    }
    
    func saveData() {
        do {
            try managedContext.save()
        } catch {
            print("Could Not Save Data")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
