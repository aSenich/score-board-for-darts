//
//  Router.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/27/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//
/*
import UIKit

class Router: UIViewController {

    
    var goodToSegue = true
    // MARK: Outlets
    @IBOutlet weak var titleMessage: UILabel!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    @IBAction func selectAnotherNumber(_ sender: Any) {
        performSegue(withIdentifier: "selectAnother", sender: self)
    }
    
    @IBAction func back(_ sender: Any) {
        numberOfNumbersSelectected = numberOfNumbersSelectected-1
        switch fromNumber {
        case 20:

            switch currentPlayer {
            case 1:
                p1twenty = p1twenty - hitValue
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2twenty = p2twenty - hitValue
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3twenty = p3twenty - hitValue
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4twenty = p4twenty - hitValue
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            
           //---------------------
            
        case 19:
            
            switch currentPlayer {
            case 1:
                p1nineteen = p1nineteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2nineteen = p2nineteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3nineteen = p3nineteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4nineteen = p4nineteen - hitValue
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            //-----------------------
        case 18:
            
            switch currentPlayer {
            case 1:
                p1eightteen = p1eightteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2eightteen = p2eightteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3eightteen = p3eightteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4eightteen = p4eightteen - hitValue
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            //------------------------------
        case 17:
            
            switch currentPlayer {
            case 1:
                p1seventeen = p1seventeen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2seventeen = p2seventeen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3seventeen = p3seventeen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4seventeen = p4seventeen - hitValue
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            //------------------------
        case 16:
            
            switch currentPlayer {
            case 1:
                p1sixteen = p1sixteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2sixteen = p2sixteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3sixteen = p3sixteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4sixteen = p4sixteen - hitValue
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            //---------------------------
        case 15:
            
            switch currentPlayer {
            case 1:
                p1fifteen = p1fifteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2fifteen = p2fifteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3fifteen = p3fifteen - hitValue
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4fifteen = p4fifteen - hitValue
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            //----------------------
        case 25:
            
            switch currentPlayer {
            case 1:
                p1bull = p1bull - hitValue
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2bull = p2bull - hitValue
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3bull = p3bull - hitValue
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4bull = p4bull - hitValue
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            
        default:
            break
        }
    }
    
    @IBAction func one(_ sender: Any) {
        numberofNumbershit = 1
    }
    
    @IBAction func two(_ sender: Any) {
        numberofNumbershit = 2
    }
    
    @IBAction func three(_ sender: Any) {
        numberofNumbershit = 3
    }
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        goodToSegue = true
        
        if numberOfNumbersSelectected == 3 {
            titleMessage.isHidden = true
            yesBtn.isHidden = true
            noBtn.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if numberOfNumbersSelectected == 3 && goodToSegue == true {
            goodToSegue = false
            performSegue(withIdentifier: "toScoreboard", sender: self)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
*/
