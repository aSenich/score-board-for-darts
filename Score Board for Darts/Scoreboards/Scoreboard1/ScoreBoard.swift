//
//  ScoreBoard.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/9/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit
import CoreData

class ScoreBoard: UIViewController {
    
    var managedContext: NSManagedObjectContext!
    
    var p1: PlayerInfo!
    var p2: PlayerInfo!
    var p3: PlayerInfo!
    var p4: PlayerInfo!
    
    var gameInfo: CurrentGameInfo!

    var player1TotalScore = 0
    var player2TotalScore = 0
    var player3TotalSocre = 0
    var player4TotalScore = 0
    
    var hideBackBtn = false
    
    
    // MARK: Actions
    
    @IBAction func backBtn(_ sender: Any) {
        gameInfo.numberOfNumsSelected = gameInfo.numberOfNumsSelected-1
        switch gameInfo.fromNumber {
        case 20:
            
            switch gameInfo.currentPlayer {
            case 1:
                p1.twenety = p1.twenety - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2.twenety = p2.twenety - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3.twenety = p3.twenety - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4.twenety = p4.twenety - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            
            //---------------------
            
        case 19:
            
            switch gameInfo.currentPlayer {
            case 1:
                p1.nineteen = p1.nineteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2.nineteen = p2.nineteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3.nineteen = p3.nineteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4.nineteen = p4.nineteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
        //-----------------------
        case 18:
            
            switch gameInfo.currentPlayer {
            case 1:
                p1.eighteen = p1.eighteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2.eighteen = p2.eighteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3.eighteen = p3.eighteen - gameInfo.hitValue
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4.eighteen = p4.eighteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
        //------------------------------
        case 17:
            
            switch gameInfo.currentPlayer {
            case 1:
                p1.seventeen = p1.seventeen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2.seventeen = p2.seventeen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3.seventeen = p3.seventeen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4.seventeen = p4.seventeen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
        //------------------------
        case 16:
            
            switch gameInfo.currentPlayer {
            case 1:
                p1.sixteen = p1.sixteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2.sixteen = p2.sixteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3.sixteen = p3.sixteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4.sixteen = p4.sixteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
        //---------------------------
        case 15:
            
            switch gameInfo.currentPlayer {
            case 1:
                p1.fifteen = p1.fifteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2.fifteen = p2.fifteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3.fifteen = p3.fifteen - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4.fifteen = p4.fifteen - gameInfo.hitValue
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
        //----------------------
        case 25:
            
            switch gameInfo.currentPlayer {
            case 1:
                p1.bull = p1.bull - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 2:
                p2.bull = p2.bull - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 3:
                p3.bull = p3.bull - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            case 4:
                p4.bull = p4.bull - gameInfo.hitValue
                saveData()
                fetch()
                self.dismiss(animated: true, completion: nil)
            default:
                break
            }
            
        default:
            break
        }
        
        
    }
    
    @IBAction func nextPlayer(_ sender: Any) {

        
        gameInfo.currentPlayer = gameInfo.currentPlayer+1
        saveData()
        fetch()
        switch gameInfo.numberOfPlayers {
        case 2:
            if gameInfo.currentPlayer == 3 {
                gameInfo.currentPlayer = 1
                saveData()
                fetch()
            }
        case 3:
            if gameInfo.currentPlayer == 4 {
                gameInfo.currentPlayer = 1
                saveData()
                fetch()
            }
        case 4:
            if gameInfo.currentPlayer == 5 {
                gameInfo.currentPlayer = 1
                saveData()
                fetch()
            }
        default:
            break
        }
        
        gameInfo.numberOfNumsSelected = 0
        saveData()
        fetch()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refresh"), object: nil)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hideUndo"), object: nil)

     //   performSegue(withIdentifier: "nextPlayer", sender: self)
        performSegue(withIdentifier: "nextPlayerTest", sender: self)
    }
    
    @IBOutlet weak var backBtn: UIButton!
    
    // MARK: Name and Total Score Text Outlets
    @IBOutlet weak var name1Text: UILabel!
    @IBOutlet weak var name2Text: UILabel!
    @IBOutlet weak var Name3Text: UILabel!
    @IBOutlet weak var name4Text: UILabel!
    
    @IBOutlet weak var p1TotalScore: UILabel!
    @IBOutlet weak var p2TotalScore: UILabel!
    @IBOutlet weak var p3TotalScore: UILabel!
    @IBOutlet weak var p4TotalScore: UILabel!
    
    // MARK: Scores marks Outlets
    
    // Player1
    //20
    @IBOutlet weak var p1twentyLeft: UILabel!
    @IBOutlet weak var p1twentyCenter: UILabel!
    @IBOutlet weak var p1twentyRight: UILabel!
    
    @IBOutlet weak var p1twentyNum: UILabel!
    
    //19
    @IBOutlet weak var p1nineteenLeft: UILabel!
    @IBOutlet weak var p1nineteenCenter: UILabel!
    @IBOutlet weak var p1nineteenRight: UILabel!
    
    @IBOutlet weak var p1nineteenNum: UILabel!
    
    //18
    @IBOutlet weak var p1eightteenLeft: UILabel!
    @IBOutlet weak var p1eightteenCenter: UILabel!
    @IBOutlet weak var p1eightteenRight: UILabel!
    
    @IBOutlet weak var p1eightteenNum: UILabel!
    
    //17
    @IBOutlet weak var p1seventeenLeft: UILabel!
    @IBOutlet weak var p1seventeenCenter: UILabel!
    @IBOutlet weak var p1seventeenRight: UILabel!
    
    @IBOutlet weak var p1seventeenNum: UILabel!
    
    //16
    @IBOutlet weak var p1sixteenLeft: UILabel!
    @IBOutlet weak var p1sixteenCenter: UILabel!
    @IBOutlet weak var p1sixteenRight: UILabel!
    
    @IBOutlet weak var p1sixteenNum: UILabel!
    
    //15
    @IBOutlet weak var p1fifteenLeft: UILabel!
    @IBOutlet weak var p1fifteenCenter: UILabel!
    @IBOutlet weak var p1fifteenRight: UILabel!
    
    
    @IBOutlet weak var p1fifteenNum: UILabel!
    //Bull
    @IBOutlet weak var p1bullLeft: UILabel!
    @IBOutlet weak var p1bullCenter: UILabel!
    @IBOutlet weak var p1bullRight: UILabel!
    
    @IBOutlet weak var p1bullNum: UILabel!
    
    
    // Player 2
    //20
    @IBOutlet weak var p2twentyLeft: UILabel!
    @IBOutlet weak var p2twentyCenter: UILabel!
    @IBOutlet weak var p2twentyRight: UILabel!
    
    @IBOutlet weak var p2twentyNum: UILabel!
    
    //19
    @IBOutlet weak var p2nineteenLeft: UILabel!
    @IBOutlet weak var p2nineteenCenter: UILabel!
    @IBOutlet weak var p2nineteenRight: UILabel!
    
    @IBOutlet weak var p2nineteenNum: UILabel!
    
    //18
    @IBOutlet weak var p2eightteenLeft: UILabel!
    @IBOutlet weak var p2eightteenCenter: UILabel!
    @IBOutlet weak var p2eightteenRight: UILabel!
    
    @IBOutlet weak var p2eightteenNum: UILabel!
    
    //17
    @IBOutlet weak var p2seventeenLeft: UILabel!
    @IBOutlet weak var p2seventeenCenter: UILabel!
    @IBOutlet weak var p2seventeenRight: UILabel!
    
    @IBOutlet weak var p2seventeenNum: UILabel!
    
    //16
    @IBOutlet weak var p2sixteenLeft: UILabel!
    @IBOutlet weak var p2sixteenCenter: UILabel!
    @IBOutlet weak var p2sixteenRight: UILabel!
    
    @IBOutlet weak var p2sixteenNum: UILabel!
    
    //15
    @IBOutlet weak var p2fifteenLeft: UILabel!
    @IBOutlet weak var p2fifteenCenter: UILabel!
    @IBOutlet weak var p2fifteenRight: UILabel!
    
    @IBOutlet weak var p2fifteenNum: UILabel!
    
    //bull
    @IBOutlet weak var p2bullLeft: UILabel!
    @IBOutlet weak var p2bullCenter: UILabel!
    @IBOutlet weak var p2bullRight: UILabel!
    
    
    @IBOutlet weak var p2bullNum: UILabel!
    
    // Player 3
    //20
    @IBOutlet weak var p3twentyLeft: UILabel!
    @IBOutlet weak var p3twentyCenter: UILabel!
    @IBOutlet weak var p3twentyRight: UILabel!
    
    @IBOutlet weak var p3twentyNum: UILabel!
    
    //19
    @IBOutlet weak var p3nineteenLeft: UILabel!
    @IBOutlet weak var p3nneteenCenter: UILabel!
    @IBOutlet weak var p3nineteenRight: UILabel!
    
    @IBOutlet weak var p3nineteenNum: UILabel!
    
    //18
    @IBOutlet weak var p3eightteenLeft: UILabel!
    @IBOutlet weak var p3eightteenCenter: UILabel!
    @IBOutlet weak var p3eightteenRight: UILabel!
    
    @IBOutlet weak var p3eightteenNum: UILabel!
    
    //17
    @IBOutlet weak var p3seventeenLeft: UILabel!
    @IBOutlet weak var p3seventeenCenter: UILabel!
    @IBOutlet weak var p3seventeenRight: UILabel!
    
    @IBOutlet weak var p3seventeenNum: UILabel!
    
    
    //16
    @IBOutlet weak var p3sixteenLeft: UILabel!
    @IBOutlet weak var p3sixteenCenter: UILabel!
    @IBOutlet weak var p3sixteenRight: UILabel!
    
    @IBOutlet weak var p3sixteenNum: UILabel!
    
    //15
    @IBOutlet weak var p3fifteenLeft: UILabel!
    @IBOutlet weak var p3fifteenCenter: UILabel!
    @IBOutlet weak var p3fifteenRight: UILabel!
    
    @IBOutlet weak var p3fifteenNum: UILabel!
    
    //bull
    @IBOutlet weak var p3bullLeft: UILabel!
    @IBOutlet weak var p3bullCenter: UILabel!
    @IBOutlet weak var p3bullRight: UILabel!
    
    
    @IBOutlet weak var p3bullNum: UILabel!
    
    // Player 4
    //20
    @IBOutlet weak var p4twentyLeft: UILabel!
    @IBOutlet weak var p4twentyCenter: UILabel!
    @IBOutlet weak var p4twentyRight: UILabel!
    
    @IBOutlet weak var p4twentyNum: UILabel!
    
    //19
    @IBOutlet weak var p4nineteenLeft: UILabel!
    @IBOutlet weak var p4nineteenCenter: UILabel!
    @IBOutlet weak var p4nineteenRight: UILabel!
    
    @IBOutlet weak var p4nineteenNum: UILabel!
    
    //18
    @IBOutlet weak var p4eightteenLeft: UILabel!
    @IBOutlet weak var p4eightteenCenter: UILabel!
    @IBOutlet weak var p4eightteenRight: UILabel!
    
    @IBOutlet weak var p4eightteenNum: UILabel!
    
    //17
    @IBOutlet weak var p4seventeenLeft: UILabel!
    @IBOutlet weak var p4seventeenCenter: UILabel!
    @IBOutlet weak var p4seventeenRight: UILabel!
    
    @IBOutlet weak var p4seventeenNum: UILabel!
    
    //16
    @IBOutlet weak var p4sixteenLeft: UILabel!
    @IBOutlet weak var p4sixteenCenter: UILabel!
    @IBOutlet weak var p4sixteenRight: UILabel!
    
    
    @IBOutlet weak var p4sixteenNum: UILabel!
    //15
    @IBOutlet weak var p4fifteenLeft: UILabel!
    @IBOutlet weak var p4fifteenCenter: UILabel!
    @IBOutlet weak var p4fifteenRight: UILabel!
    
    @IBOutlet weak var p4fifteenNum: UILabel!
    
    //bull
    @IBOutlet weak var p4bullLeft: UILabel!
    @IBOutlet weak var p4bullCenter: UILabel!
    @IBOutlet weak var p4bullRight: UILabel!
    
    @IBOutlet weak var p4bullNum: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        gameInfo.currentLocation = 13
        saveData()
        fetch()
        totalScores()

        // Do any additional setup after loading the view.

        setUp()
    
        if hideBackBtn == true {
            backBtn.isHidden = true
            hideBackBtn = false
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        setUp2()

        
        
 
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if Int(p1.twenety) >= 3 && Int(p1.nineteen) >= 3 && Int(p1.eighteen) >= 3 && Int(p1.seventeen) >= 3 && Int(p1.sixteen) >= 3 && Int(p1.fifteen) >= 3 && Int(p1.bull) >= 3 && player1TotalScore > player2TotalScore && player1TotalScore > player3TotalSocre && player1TotalScore > player4TotalScore {
            
            gameInfo.winningPlayer = p1.name!
            saveData()
            fetch()
            performSegue(withIdentifier: "winnerView", sender: self)
        }
        
        if Int(p2.twenety) >= 3 && Int(p2.nineteen) >= 3 && Int(p2.eighteen) >= 3 && Int(p2.seventeen) >= 3 && Int(p2.sixteen) >= 3 && Int(p2.fifteen) >= 3 && Int(p2.bull) >= 3 && player2TotalScore > player1TotalScore && player2TotalScore > player3TotalSocre && player2TotalScore > player4TotalScore {
            gameInfo.winningPlayer = p2.name!
            saveData()
            fetch()
            performSegue(withIdentifier: "winnerView", sender: self)
        }
        
        if Int(p3.twenety) >= 3 && Int(p3.nineteen) >= 3 && Int(p3.eighteen) >= 3 && Int(p3.seventeen) >= 3 && Int(p3.sixteen) >= 3 && Int(p3.fifteen) >= 3 && Int(p3.bull) >= 3 && player3TotalSocre > player1TotalScore && player3TotalSocre > player2TotalScore && player3TotalSocre > player4TotalScore {
            gameInfo.winningPlayer = p3.name!
            saveData()
            fetch()
            performSegue(withIdentifier: "winnerView", sender: self)
        }
        
        if Int(p4.twenety) >= 3 && Int(p4.nineteen) >= 3 && Int(p4.eighteen) >= 3 && Int(p4.seventeen) >= 3 && Int(p4.sixteen) >= 3 && Int(p4.fifteen) >= 3 && Int(p4.bull) >= 3 && player4TotalScore > player1TotalScore && player4TotalScore > player2TotalScore && player4TotalScore > player3TotalSocre {
            gameInfo.winningPlayer = p4.name!
            saveData()
            fetch()
            performSegue(withIdentifier: "winnerView", sender: self)
        }
    }
    
    
    
    
    
   
   
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "winnerView" {
            let winningViewController = segue.destination as! winner
            
            winningViewController.managedContext = managedContext
        }
    }

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


