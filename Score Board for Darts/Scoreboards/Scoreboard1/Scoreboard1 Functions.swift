//
//  Scoreboar1 Functions.swift
//  Score Board for Darts
//
//  Created by Mark Senich on 1/28/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import UIKit
import CoreData

extension ScoreBoard {
    
    func totalScores() {
        switch gameInfo.numberOfPlayers {
        case 2:
            let p1scorePart1 = (Int(p1.twenety) * 20) + (Int(p1.nineteen) * 19) + (Int(p1.eighteen) * 18)
            let p1scorePart2 = (Int(p1.seventeen) * 17) + (Int(p1.sixteen) * 16) + (Int(p1.fifteen) * 15) + (Int(p1.bull) * 25)
            player1TotalScore = p1scorePart1 + p1scorePart2
            
            let p2scorePart1 = (Int(p2.twenety) * 20) + (Int(p2.nineteen) * 19) + (Int(p2.eighteen) * 18)
            let p2scorePart2 = (Int(p2.seventeen) * 17) + (Int(p2.sixteen) * 16) + (Int(p2.fifteen) * 15) + (Int(p2.bull) * 25)
            player2TotalScore = p2scorePart1 + p2scorePart2
            
        case 3:
            let p1scorePart1 = (Int(p1.twenety) * 20) + (Int(p1.nineteen) * 19) + (Int(p1.eighteen) * 18)
            let p1scorePart2 = (Int(p1.seventeen) * 17) + (Int(p1.sixteen) * 16) + (Int(p1.fifteen) * 15) + (Int(p1.bull) * 25)
            player1TotalScore = p1scorePart1 + p1scorePart2
            
            let p2scorePart1 = (Int(p2.twenety) * 20) + (Int(p2.nineteen) * 19) + (Int(p2.eighteen) * 18)
            let p2scorePart2 = (Int(p2.seventeen) * 17) + (Int(p2.sixteen) * 16) + (Int(p2.fifteen) * 15) + (Int(p2.bull) * 25)
            player2TotalScore = p2scorePart1 + p2scorePart2
            
            let p3scorePart1 = (Int(p3.twenety) * 20) + (Int(p3.nineteen) * 19) + (Int(p3.eighteen) * 18)
            let p3scorePart2 = (Int(p3.seventeen) * 17) + (Int(p3.sixteen) * 16) + (Int(p3.fifteen) * 15) + (Int(p3.bull) * 25)
            player3TotalSocre = p3scorePart1 + p3scorePart2
            
        case 4:
            let p1scorePart1 = (Int(p1.twenety) * 20) + (Int(p1.nineteen) * 19) + (Int(p1.eighteen) * 18)
            let p1scorePart2 = (Int(p1.seventeen) * 17) + (Int(p1.sixteen) * 16) + (Int(p1.fifteen) * 15) + (Int(p1.bull) * 25)
            player1TotalScore = p1scorePart1 + p1scorePart2
            
            let p2scorePart1 = (Int(p2.twenety) * 20) + (Int(p2.nineteen) * 19) + (Int(p2.eighteen) * 18)
            let p2scorePart2 = (Int(p2.seventeen) * 17) + (Int(p2.sixteen) * 16) + (Int(p2.fifteen) * 15) + (Int(p2.bull) * 25)
            player2TotalScore = p2scorePart1 + p2scorePart2
            
            let p3scorePart1 = (Int(p3.twenety) * 20) + (Int(p3.nineteen) * 19) + (Int(p3.eighteen) * 18)
            let p3scorePart2 = (Int(p3.seventeen) * 17) + (Int(p3.sixteen) * 16) + (Int(p3.fifteen) * 15) + (Int(p3.bull) * 25)
            player3TotalSocre = p3scorePart1 + p3scorePart2
            
            let p4scorePart1 = (Int(p4.twenety) * 20) + (Int(p4.nineteen) * 19) + (Int(p4.eighteen) * 18)
            let p4scorePart2 = (Int(p4.seventeen) * 17) + (Int(p4.sixteen) * 16) + (Int(p4.fifteen) * 15) + (Int(p4.bull) * 25)
            player4TotalScore = p4scorePart1 + p4scorePart2
            
        default:
            break
        }
    }
    
    
    func refreshDartboard() {
        let storyboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let dartboard: ViewController3 = storyboard.instantiateViewController(withIdentifier: "ViewController3") as! ViewController3
        
        dartboard.refresh()
    }
    
    
    func setUp() {
        // Clears the scoreboard so it can display players current score.
        
        // Player1
        p1twentyLeft.isHidden = true
        p1twentyCenter.isHidden = true
        p1twentyRight.isHidden = true
        
        p1nineteenLeft.isHidden = true
        p1nineteenCenter.isHidden = true
        p1nineteenRight.isHidden = true
        
        p1eightteenLeft.isHidden = true
        p1eightteenCenter.isHidden = true
        p1eightteenRight.isHidden = true
        
        p1seventeenLeft.isHidden = true
        p1seventeenCenter.isHidden = true
        p1seventeenRight.isHidden = true
        
        p1sixteenLeft.isHidden = true
        p1sixteenCenter.isHidden = true
        p1sixteenRight.isHidden = true
        
        p1fifteenLeft.isHidden = true
        p1fifteenCenter.isHidden = true
        p1fifteenRight.isHidden = true
        
        p1bullLeft.isHidden = true
        p1bullCenter.isHidden = true
        p1bullRight.isHidden = true
        
        // Player2
        p2twentyLeft.isHidden = true
        p2twentyCenter.isHidden = true
        p2twentyRight.isHidden = true
        
        p2nineteenLeft.isHidden = true
        p2nineteenCenter.isHidden = true
        p2nineteenRight.isHidden = true
        
        p2eightteenLeft.isHidden = true
        p2eightteenCenter.isHidden = true
        p2eightteenRight.isHidden = true
        
        p2seventeenLeft.isHidden = true
        p2seventeenCenter.isHidden = true
        p2seventeenRight.isHidden = true
        
        p2sixteenLeft.isHidden = true
        p2sixteenCenter.isHidden = true
        p2sixteenRight.isHidden = true
        
        p2fifteenLeft.isHidden = true
        p2fifteenCenter.isHidden = true
        p2fifteenRight.isHidden = true
        
        p2bullLeft.isHidden = true
        p2bullCenter.isHidden = true
        p2bullRight.isHidden = true
        
        // Player3
        p3twentyLeft.isHidden = true
        p3twentyCenter.isHidden = true
        p3twentyRight.isHidden = true
        
        p3nineteenLeft.isHidden = true
        p3nneteenCenter.isHidden = true
        p3nineteenRight.isHidden = true
        
        p3eightteenLeft.isHidden = true
        p3eightteenCenter.isHidden = true
        p3eightteenRight.isHidden = true
        
        p3seventeenLeft.isHidden = true
        p3seventeenCenter.isHidden = true
        p3seventeenRight.isHidden = true
        
        p3sixteenLeft.isHidden = true
        p3sixteenCenter.isHidden = true
        p3sixteenRight.isHidden = true
        
        p3fifteenLeft.isHidden = true
        p3fifteenCenter.isHidden = true
        p3fifteenRight.isHidden = true
        
        p3bullLeft.isHidden = true
        p3bullCenter.isHidden = true
        p3bullRight.isHidden = true
        
        // Player4
        p4twentyLeft.isHidden = true
        p4twentyCenter.isHidden = true
        p4twentyRight.isHidden = true
        
        p4nineteenLeft.isHidden = true
        p4nineteenCenter.isHidden = true
        p4nineteenRight.isHidden = true
        
        p4eightteenLeft.isHidden = true
        p4eightteenCenter.isHidden = true
        p4eightteenRight.isHidden = true
        
        p4seventeenLeft.isHidden = true
        p4seventeenCenter.isHidden = true
        p4seventeenRight.isHidden = true
        
        p4sixteenLeft.isHidden = true
        p4sixteenCenter.isHidden = true
        p4sixteenRight.isHidden = true
        
        p4fifteenLeft.isHidden = true
        p4fifteenCenter.isHidden = true
        p4fifteenRight.isHidden = true
        
        p4bullLeft.isHidden = true
        p4bullCenter.isHidden = true
        p4bullRight.isHidden = true
        
        
        // Determines if a player has closed out a number
        // Player 1
        if p2.twentyClosed == false && p3.twentyClosed == false && p4.twentyClosed == false {
            
            if Int(p1.twenety) == 3 || Int(p1.twenety)>3 {
                p1.twentyClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p2.nineteenClosed == false && p3.nineteenClosed == false && p4.nineteenClosed == false {
            
            if Int(p1.nineteen) == 3 || Int(p1.nineteen)>3 {
                p1.nineteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p2.eighteenClosed == false && p3.eighteenClosed == false && p4.eighteenClosed == false {
            
            if Int(p1.eighteen) == 3 || Int(p1.eighteen)>3 {
                p1.eighteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p2.seventeenClosed == false && p3.seventeenClosed == false && p4.seventeenClosed == false {
            
            if Int(p1.seventeen) == 3 || Int(p1.seventeen)>3 {
                p1.seventeenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p2.sixteenClosed == false && p3.sixteenClosed == false && p4.sixteenClosed == false {
            
            if Int(p1.sixteen) == 3 || Int(p1.sixteen)>3 {
                p1.sixteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p2.fifteenClosed == false && p3.fifteenClosed == false && p4.fifteenClosed == false {
            
            if Int(p1.fifteen) == 3 || Int(p1.fifteen)>3 {
                p1.fifteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p2.bullClosed == false && p3.bullClosed == false && p4.bullClosed == false {
            
            if Int(p1.bull) == 3 || Int(p1.bull)>3 {
                p1.bullClosed = true
                saveData()
                fetch()
            }
        }
        
        // Player 2
        if p1.twentyClosed == false && p3.twentyClosed == false && p4.twentyClosed == false {
            
            if Int(p2.twenety) == 3 || Int(p2.twenety)>3 {
                p2.twentyClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.nineteenClosed == false && p3.nineteenClosed == false && p4.nineteenClosed == false {
            
            if Int(p2.nineteen) == 3 || Int(p2.nineteen)>3 {
                p2.nineteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.eighteenClosed == false && p3.eighteenClosed == false && p4.eighteenClosed == false {
            
            if Int(p2.eighteen) == 3 || Int(p2.eighteen)>3 {
                p2.eighteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.seventeenClosed == false && p3.seventeenClosed == false && p4.seventeenClosed == false {
            
            if Int(p2.seventeen) == 3 || Int(p2.seventeen)>3 {
                p2.seventeenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.sixteenClosed == false && p3.sixteenClosed == false && p4.sixteenClosed == false {
            
            if Int(p2.sixteen) == 3 || Int(p2.sixteen)>3 {
                p2.sixteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.fifteenClosed == false && p3.fifteenClosed == false && p4.fifteenClosed == false {
            
            if Int(p2.fifteen) == 3 || Int(p2.fifteen)>3 {
                p2.fifteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.bullClosed == false && p3.bullClosed == false && p4.bullClosed == false {
            
            if Int(p2.bull) == 3 || Int(p2.bull)>3 {
                p2.bullClosed = true
                saveData()
                fetch()
            }
        }
        
        // Player 3
        
        if p1.twentyClosed == false && p2.twentyClosed == false && p4.twentyClosed == false {
            
            if Int(p3.twenety) == 3 || Int(p3.twenety)>3 {
                p3.twentyClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.nineteenClosed == false && p2.nineteenClosed == false && p4.nineteenClosed == false {
            
            if Int(p3.nineteen) == 3 || Int(p3.nineteen)>3 {
                p3.nineteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.eighteenClosed == false && p2.eighteenClosed == false && p4.eighteenClosed == false {
            
            if Int(p3.eighteen) == 3 || Int(p3.eighteen)>3 {
                p3.eighteenClosed = true
            }
        }
        //
        if p1.seventeenClosed == false && p2.seventeenClosed == false && p4.seventeenClosed == false {
            
            if Int(p3.seventeen) == 3 || Int(p3.seventeen)>3 {
                p3.seventeenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.sixteenClosed == false && p2.sixteenClosed == false && p4.sixteenClosed == false {
            
            if Int(p3.sixteen) == 3 || Int(p3.sixteen)>3 {
                p3.sixteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.fifteenClosed == false && p2.fifteenClosed == false && p4.fifteenClosed == false {
            
            if Int(p3.fifteen) == 3 || Int(p3.fifteen)>3 {
                p3.fifteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.bullClosed == false && p2.bullClosed == false && p4.bullClosed == false {
            
            if Int(p3.bull) == 3 || Int(p3.bull)>3 {
                p3.bullClosed = true
                saveData()
                fetch()
            }
        }
        
        // Player 4
        if p1.twentyClosed == false && p2.twentyClosed == false && p3.twentyClosed == false {
            
            if Int(p4.twenety) == 3 || Int(p4.twenety)>3 {
                p4.twentyClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.nineteenClosed == false && p2.nineteenClosed == false && p3.nineteenClosed == false {
            
            if Int(p4.nineteen) == 3 || Int(p4.nineteen)>3 {
                p4.nineteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.eighteenClosed == false && p2.eighteenClosed == false && p3.eighteenClosed == false {
            
            if Int(p4.eighteen) == 3 || Int(p4.eighteen)>3 {
                p4.eighteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.seventeenClosed == false && p2.seventeenClosed == false && p3.seventeenClosed == false {
            
            if Int(p4.seventeen) == 3 || Int(p4.seventeen)>3 {
                p4.seventeenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.sixteenClosed == false && p2.sixteenClosed == false && p3.sixteenClosed == false {
            
            if Int(p4.sixteen) == 3 || Int(p4.sixteen)>3 {
                p4.sixteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.fifteenClosed == false && p2.fifteenClosed == false && p3.fifteenClosed == false {
            
            if Int(p4.fifteen) == 3 || Int(p4.fifteen)>3 {
                p4.fifteenClosed = true
                saveData()
                fetch()
            }
        }
        //
        if p1.bullClosed == false && p2.bullClosed == false && p3.bullClosed == false {
            
            if Int(p4.bull) == 3 || Int(p4.bull)>3 {
                p4.bullClosed = true
                saveData()
                fetch()
            }
        }
        
        // Prevents other players from gaining points if number is already closed out.
        
        // Player1
        //20
        if p2.twentyClosed == true || p3.twentyClosed == true || p4.twentyClosed == true {
            
            if Int(p1.twenety)>3 {
                p1.twenety = 3
                saveData()
                fetch()
            }
        }
        //19
        if p2.nineteenClosed == true || p3.nineteenClosed == true || p4.nineteenClosed == true {
            
            if Int(p1.nineteen)>3 {
                p1.nineteen = 3
                saveData()
                fetch()
            }
        }
        //18
        if p2.eighteenClosed == true || p3.eighteenClosed == true || p4.eighteenClosed == true {
            
            if Int(p1.eighteen)>3 {
                p1.eighteen = 3
                saveData()
                fetch()
            }
        }
        //17
        if p2.seventeenClosed == true || p3.seventeenClosed == true || p4.seventeenClosed == true {
            
            if Int(p1.seventeen)>3 {
                p1.seventeen = 3
                saveData()
                fetch()
            }
        }
        
        //16
        if p2.sixteenClosed == true || p3.sixteenClosed == true || p4.sixteenClosed == true {
            
            if Int(p1.sixteen)>3 {
                p1.sixteen = 3
                saveData()
                fetch()
            }
        }
        
        //15
        if p2.fifteenClosed == true || p3.fifteenClosed == true || p4.fifteenClosed == true {
            
            if Int(p1.fifteen)>3 {
                p1.fifteen = 3
                saveData()
                fetch()
            }
        }
        
        //bull
        if p2.bullClosed == true || p3.bullClosed == true || p4.bullClosed == true {
            
            if Int(p1.bull)>3 {
                p1.bull = 3
                saveData()
                fetch()
            }
        }
        
        // Player 2
        //20
        if p1.twentyClosed == true || p3.twentyClosed == true || p4.twentyClosed == true {
            
            if Int(p2.twenety)>3 {
                p2.twenety = 3
                saveData()
                fetch()
            }
        }
        //19
        if p1.nineteenClosed == true || p3.nineteenClosed == true || p4.nineteenClosed == true {
            
            if Int(p2.nineteen)>3 {
                p2.nineteen = 3
                saveData()
                fetch()
            }
        }
        //18
        if p1.eighteenClosed == true || p3.eighteenClosed == true || p4.eighteenClosed == true {
            
            if Int(p2.eighteen)>3 {
                p2.eighteen = 3
                saveData()
                fetch()
            }
        }
        //17
        if p1.seventeenClosed == true || p3.seventeenClosed == true || p4.seventeenClosed == true {
            
            if Int(p2.seventeen)>3 {
                p2.seventeen = 3
                saveData()
                fetch()
            }
        }
        
        //16
        if p1.sixteenClosed == true || p3.sixteenClosed == true || p4.sixteenClosed == true {
            
            if Int(p2.sixteen)>3 {
                p2.sixteen = 3
                saveData()
                fetch()
            }
        }
        
        //15
        if p1.fifteenClosed == true || p3.fifteenClosed == true || p4.fifteenClosed == true {
            
            if Int(p2.fifteen)>3 {
                p2.fifteen = 3
                saveData()
                fetch()
            }
        }
        
        //bull
        if p1.bullClosed == true || p3.bullClosed == true || p4.bullClosed == true {
            
            if Int(p2.bull)>3 {
                p2.bull = 3
                saveData()
                fetch()
            }
        }
        
        // Player 3
        //20
        if p2.twentyClosed == true || p1.twentyClosed == true || p4.twentyClosed == true {
            
            if Int(p3.twenety)>3 {
                p3.twenety = 3
                saveData()
                fetch()
            }
        }
        //19
        if p2.nineteenClosed == true || p1.nineteenClosed == true || p4.nineteenClosed == true {
            
            if Int(p3.nineteen)>3 {
                p3.nineteen = 3
                saveData()
                fetch()
            }
        }
        //18
        if p2.eighteenClosed == true || p1.eighteenClosed == true || p4.eighteenClosed == true {
            
            if Int(p3.eighteen)>3 {
                p3.eighteen = 3
                saveData()
                fetch()
            }
        }
        //17
        if p2.seventeenClosed == true || p1.seventeenClosed == true || p4.seventeenClosed == true {
            
            if Int(p3.seventeen)>3 {
                p3.seventeen = 3
                saveData()
                fetch()
            }
        }
        
        //16
        if p2.sixteenClosed == true || p1.sixteenClosed == true || p4.sixteenClosed == true {
            
            if Int(p3.sixteen)>3 {
                p3.sixteen = 3
                saveData()
                fetch()
            }
        }
        
        //15
        if p2.fifteenClosed == true || p1.fifteenClosed == true || p4.fifteenClosed == true {
            
            if Int(p3.fifteen)>3 {
                p3.fifteen = 3
                saveData()
                fetch()
            }
        }
        
        //bull
        if p2.bullClosed == true || p1.bullClosed == true || p4.bullClosed == true {
            
            if Int(p3.bull)>3 {
                p3.bull = 3
                saveData()
                fetch()
            }
        }
        
        // Player 4
        //20
        if p2.twentyClosed == true || p3.twentyClosed == true || p1.twentyClosed == true {
            
            if Int(p4.twenety)>3 {
                p4.twenety = 3
                saveData()
                fetch()
            }
        }
        //19
        if p2.nineteenClosed == true || p3.nineteenClosed == true || p1.nineteenClosed == true {
            
            if Int(p4.nineteen)>3 {
                p4.nineteen = 3
                saveData()
                fetch()
            }
        }
        //18
        if p2.eighteenClosed == true || p3.eighteenClosed == true || p1.eighteenClosed == true {
            
            if Int(p4.eighteen)>3 {
                p4.eighteen = 3
                saveData()
                fetch()
            }
        }
        //17
        if p2.seventeenClosed == true || p3.seventeenClosed == true || p1.seventeenClosed == true {
            
            if Int(p4.seventeen)>3 {
                p4.seventeen = 3
                saveData()
                fetch()
            }
        }
        
        //16
        if p2.sixteenClosed == true || p3.sixteenClosed == true || p1.sixteenClosed == true {
            
            if Int(p4.sixteen)>3 {
                p4.sixteen = 3
                saveData()
                fetch()
            }
        }
        
        //15
        if p2.fifteenClosed == true || p3.fifteenClosed == true || p1.fifteenClosed == true {
            
            if Int(p4.fifteen)>3 {
                p4.fifteen = 3
            }
        }
        
        //bull
        if p2.bullClosed == true || p3.bullClosed == true || p1.bullClosed == true {
            
            if Int(p4.bull)>3 {
                p4.bull = 3
                saveData()
                fetch()
            }
            
            
            
        }
        
        switch gameInfo.currentPlayer {
        case 1:
            name1Text.textColor = UIColor.yellow
        case 2:
            name2Text.textColor = UIColor.yellow
        case 3:
            Name3Text.textColor = UIColor.yellow
        case 4:
            name4Text.textColor = UIColor.yellow
        default:
            break
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        switch gameInfo.numberOfPlayers {
        case 2:
            name1Text.text = p1.name!
            name2Text.text = p2.name!
            Name3Text.isHidden = true
            name4Text.isHidden = true
            
            p1TotalScore.text = "\(player1TotalScore)"
            p2TotalScore.text = "\(player2TotalScore)"
            p3TotalScore.isHidden = true
            p4TotalScore.isHidden = true
        case 3:
            name1Text.text = p1.name!
            name2Text.text = p2.name!
            Name3Text.text = p3.name!
            name4Text.isHidden = true
            
            p1TotalScore.text = "\(player1TotalScore)"
            p2TotalScore.text = "\(player2TotalScore)"
            p3TotalScore.text = "\(player3TotalSocre)"
            p4TotalScore.isHidden = true
        case 4:
            name1Text.text = p1.name!
            name2Text.text = p2.name!
            Name3Text.text = p3.name!
            name4Text.text = p4.name!
            
            p1TotalScore.text = "\(player1TotalScore)"
            p2TotalScore.text = "\(player2TotalScore)"
            p3TotalScore.text = "\(player3TotalSocre)"
            p4TotalScore.text = "\(player4TotalScore)"
        default:
            print("Error in number of players tracker")
        }
    }
    
    
    
    func setUp2() {
        // Player 1
        //20
        switch Int(p1.twenety) {
        case 0:
            break
        case 1:
            p1twentyLeft.isHidden = false
        case 2:
            p1twentyLeft.isHidden = false
            p1twentyRight.isHidden = false
        case 3:
            p1twentyLeft.isHidden = false
            p1twentyCenter.isHidden = false
            p1twentyRight.isHidden = false
        default:
            if Int(p1.twenety) > 3 {
                p1twentyNum.text = "\(Int(p1.twenety) * 20)"
            }
        }
        
        
        //19
        switch Int(p1.nineteen) {
        case 0:
            break
        case 1:
            p1nineteenLeft.isHidden = false
        case 2:
            p1nineteenRight.isHidden = false
            p1nineteenLeft.isHidden = false
        case 3:
            p1nineteenCenter.isHidden = false
            p1nineteenLeft.isHidden = false
            p1nineteenRight.isHidden = false
        default:
            if Int(p1.nineteen) > 3 {
                p1nineteenNum.text = "\(Int(p1.nineteen) * 19)"
            }
        }
        //18
        switch Int(p1.eighteen) {
        case 0:
            break
        case 1:
            p1eightteenLeft.isHidden = false
        case 2:
            p1eightteenRight.isHidden = false
            p1eightteenLeft.isHidden = false
        case 3:
            p1eightteenCenter.isHidden = false
            p1eightteenLeft.isHidden = false
            p1eightteenRight.isHidden = false
        default:
            if Int(p1.eighteen) > 3 {
                p1eightteenNum.text = "\(Int(p1.eighteen) * 18)"
            }
        }
        
        //17
        switch Int(p1.seventeen) {
        case 0:
            break
        case 1:
            p1seventeenLeft.isHidden = false
        case 2:
            p1seventeenRight.isHidden = false
            p1seventeenLeft.isHidden = false
        case 3:
            p1seventeenCenter.isHidden = false
            p1seventeenLeft.isHidden = false
            p1seventeenRight.isHidden = false
        default:
            if Int(p1.seventeen) > 3 {
                p1seventeenNum.text = "\(Int(p1.seventeen) * 17)"
            }
        }
        
        //16
        switch Int(p1.sixteen) {
        case 0:
            break
        case 1:
            p1sixteenLeft.isHidden = false
        case 2:
            p1sixteenRight.isHidden = false
            p1sixteenLeft.isHidden = false
        case 3:
            p1sixteenCenter.isHidden = false
            p1sixteenLeft.isHidden = false
            p1sixteenRight.isHidden = false
        default:
            if Int(p1.sixteen) > 3 {
                p1sixteenNum.text = "\(Int(p1.sixteen) * 16)"
            }
        }
        
        //15
        switch Int(p1.fifteen) {
        case 0:
            break
        case 1:
            p1fifteenLeft.isHidden = false
        case 2:
            p1fifteenRight.isHidden = false
            p1fifteenLeft.isHidden = false
        case 3:
            p1fifteenCenter.isHidden = false
            p1fifteenLeft.isHidden = false
            p1fifteenRight.isHidden = false
        default:
            if Int(p1.fifteen) > 3 {
                p1fifteenNum.text = "\(Int(p1.fifteen) * 15)"
            }
        }
        
        //bull
        switch Int(p1.bull) {
        case 0:
            break
        case 1:
            p1bullLeft.isHidden = false
        case 2:
            p1bullRight.isHidden = false
            p1bullLeft.isHidden = false
        case 3:
            p1bullCenter.isHidden = false
            p1bullLeft.isHidden = false
            p1bullRight.isHidden = false
        default:
            if Int(p1.bull) > 3 {
                p1bullNum.text = "\(Int(p1.bull) * 25)"
            }
        }
        
        // Player 2
        //20
        switch Int(p2.twenety) {
        case 0:
            break
        case 1:
            p2twentyLeft.isHidden = false
        case 2:
            p2twentyRight.isHidden = false
            p2twentyLeft.isHidden = false
        case 3:
            p2twentyLeft.isHidden = false
            p2twentyCenter.isHidden = false
            p2twentyRight.isHidden = false
        default:
            if Int(p2.twenety) > 3 {
                p2twentyNum.text = "\(Int(p2.twenety) * 20)"
            }
        }
        
        
        //19
        switch Int(p2.nineteen) {
        case 0:
            break
        case 1:
            p2nineteenLeft.isHidden = false
        case 2:
            p2nineteenRight.isHidden = false
            p2nineteenLeft.isHidden = false
        case 3:
            p2nineteenCenter.isHidden = false
            p2nineteenLeft.isHidden = false
            p2nineteenRight.isHidden = false
        default:
            if Int(p2.nineteen) > 3 {
                p2nineteenNum.text = "\(Int(p2.nineteen) * 19)"
            }
        }
        //18
        switch Int(p2.eighteen) {
        case 0:
            break
        case 1:
            p2eightteenLeft.isHidden = false
        case 2:
            p2eightteenRight.isHidden = false
            p2eightteenLeft.isHidden = false
        case 3:
            p2eightteenCenter.isHidden = false
            p2eightteenLeft.isHidden = false
            p2eightteenRight.isHidden = false
        default:
            if Int(p2.eighteen) > 3 {
                p2eightteenNum.text = "\(Int(p2.eighteen) * 18)"
            }
        }
        
        //17
        switch Int(p2.seventeen) {
        case 0:
            break
        case 1:
            p2seventeenLeft.isHidden = false
        case 2:
            p2seventeenRight.isHidden = false
            p2seventeenLeft.isHidden = false
        case 3:
            p2seventeenCenter.isHidden = false
            p2seventeenLeft.isHidden = false
            p2seventeenRight.isHidden = false
        default:
            if Int(p2.seventeen) > 3 {
                p2seventeenNum.text = "\(Int(p2.seventeen) * 17)"
            }
        }
        
        //16
        switch Int(p2.sixteen) {
        case 0:
            break
        case 1:
            p2sixteenLeft.isHidden = false
        case 2:
            p2sixteenRight.isHidden = false
            p2sixteenLeft.isHidden = false
        case 3:
            p2sixteenCenter.isHidden = false
            p2sixteenLeft.isHidden = false
            p2sixteenRight.isHidden = false
        default:
            if Int(p2.sixteen) > 3 {
                p2sixteenNum.text = "\(Int(p2.sixteen) * 16)"
            }
        }
        
        //15
        switch Int(p2.fifteen) {
        case 0:
            break
        case 1:
            p2fifteenLeft.isHidden = false
        case 2:
            p2fifteenRight.isHidden = false
            p2fifteenLeft.isHidden = false
        case 3:
            p2fifteenCenter.isHidden = false
            p2fifteenLeft.isHidden = false
            p2fifteenRight.isHidden = false
        default:
            if Int(p2.fifteen) > 3 {
                p2fifteenNum.text = "\(Int(p2.fifteen) * 15)"
            }
        }
        
        //bull
        switch Int(p2.bull) {
        case 0:
            break
        case 1:
            p2bullLeft.isHidden = false
        case 2:
            p2bullRight.isHidden = false
            p2bullLeft.isHidden = false
        case 3:
            p2bullCenter.isHidden = false
            p2bullLeft.isHidden = false
            p2bullRight.isHidden = false
        default:
            if Int(p2.bull) > 3 {
                p2bullNum.text = "\(Int(p2.bull) * 25)"
            }
        }
        
        // Player 3
        //20
        switch Int(p3.twenety) {
        case 0:
            break
        case 1:
            p3twentyLeft.isHidden = false
        case 2:
            p3twentyLeft.isHidden = false
            p3twentyRight.isHidden = false
        case 3:
            p3twentyLeft.isHidden = false
            p3twentyCenter.isHidden = false
            p3twentyRight.isHidden = false
        default:
            if Int(p3.twenety) > 3 {
                p3twentyNum.text = "\(Int(p3.twenety) * 20)"
            }
        }
        
        
        //19
        switch Int(p3.nineteen) {
        case 0:
            break
        case 1:
            p3nineteenLeft.isHidden = false
        case 2:
            p3nineteenRight.isHidden = false
            p3nineteenLeft.isHidden = false
        case 3:
            p3nneteenCenter.isHidden = false
            p3nineteenLeft.isHidden = false
            p3nineteenRight.isHidden = false
        default:
            if Int(p3.nineteen) > 3 {
                p3nineteenNum.text = "\(Int(p3.nineteen) * 19)"
            }
        }
        //18
        switch Int(p3.eighteen) {
        case 0:
            break
        case 1:
            p3eightteenLeft.isHidden = false
        case 2:
            p3eightteenRight.isHidden = false
            p3eightteenLeft.isHidden = false
        case 3:
            p3eightteenCenter.isHidden = false
            p3eightteenLeft.isHidden = false
            p3eightteenRight.isHidden = false
        default:
            if Int(p3.eighteen) > 3 {
                p3eightteenNum.text = "\(Int(p3.eighteen) * 18)"
            }
        }
        
        //17
        switch Int(p3.seventeen) {
        case 0:
            break
        case 1:
            p3seventeenLeft.isHidden = false
        case 2:
            p3seventeenRight.isHidden = false
            p3seventeenLeft.isHidden = false
        case 3:
            p3seventeenCenter.isHidden = false
            p3seventeenLeft.isHidden = false
            p3seventeenRight.isHidden = false
        default:
            if Int(p3.seventeen) > 3 {
                p3seventeenNum.text = "\(Int(p3.seventeen) * 17)"
            }
        }
        
        //16
        switch Int(p3.sixteen) {
        case 0:
            break
        case 1:
            p3sixteenLeft.isHidden = false
        case 2:
            p3sixteenRight.isHidden = false
            p3sixteenLeft.isHidden = false
        case 3:
            p3sixteenCenter.isHidden = false
            p3sixteenLeft.isHidden = false
            p3sixteenRight.isHidden = false
        default:
            if Int(p3.sixteen) > 3 {
                p3sixteenNum.text = "\(Int(p3.sixteen) * 16)"
            }
        }
        
        //15
        switch Int(p3.fifteen) {
        case 0:
            break
        case 1:
            p3fifteenLeft.isHidden = false
        case 2:
            p3fifteenRight.isHidden = false
            p3fifteenLeft.isHidden = false
        case 3:
            p3fifteenCenter.isHidden = false
            p3fifteenLeft.isHidden = false
            p3fifteenRight.isHidden = false
        default:
            if Int(p3.fifteen) > 3 {
                p3fifteenNum.text = "\(Int(p3.fifteen) * 15)"
            }
        }
        
        //bull
        switch Int(p3.bull) {
        case 0:
            break
        case 1:
            p3bullLeft.isHidden = false
        case 2:
            p3bullRight.isHidden = false
            p3bullLeft.isHidden = false
        case 3:
            p3bullCenter.isHidden = false
            p3bullLeft.isHidden = false
            p3bullRight.isHidden = false
        default:
            if Int(p3.bull) > 3 {
                p3bullNum.text = "\(Int(p3.bull) * 25)"
            }
        }
        
        // Player 4
        //20
        switch Int(p4.twenety) {
        case 0:
            break
        case 1:
            p4twentyLeft.isHidden = false
        case 2:
            p4twentyLeft.isHidden = false
            p4twentyRight.isHidden = false
        case 3:
            p4twentyLeft.isHidden = false
            p4twentyCenter.isHidden = false
            p4twentyRight.isHidden = false
        default:
            if Int(p4.twenety) > 3 {
                p4twentyNum.text = "\(Int(p4.twenety) * 20)"
            }
        }
        
        
        //19
        switch Int(p4.nineteen) {
        case 0:
            break
        case 1:
            p4nineteenLeft.isHidden = false
        case 2:
            p4nineteenRight.isHidden = false
            p4nineteenLeft.isHidden = false
        case 3:
            p4nineteenCenter.isHidden = false
            p4nineteenLeft.isHidden = false
            p4nineteenRight.isHidden = false
        default:
            if Int(p4.nineteen) > 3 {
                p4nineteenNum.text = "\(Int(p4.nineteen) * 19)"
            }
        }
        //18
        switch Int(p4.eighteen) {
        case 0:
            break
        case 1:
            p4eightteenLeft.isHidden = false
        case 2:
            p4eightteenRight.isHidden = false
            p4eightteenLeft.isHidden = false
        case 3:
            p4eightteenCenter.isHidden = false
            p4eightteenLeft.isHidden = false
            p4eightteenRight.isHidden = false
        default:
            if Int(p4.eighteen) > 3 {
                p4eightteenNum.text = "\(Int(p4.eighteen) * 18)"
            }
        }
        
        //17
        switch Int(p4.seventeen) {
        case 0:
            break
        case 1:
            p4seventeenLeft.isHidden = false
        case 2:
            p4seventeenRight.isHidden = false
            p4seventeenLeft.isHidden = false
        case 3:
            p4seventeenCenter.isHidden = false
            p4seventeenLeft.isHidden = false
            p4seventeenRight.isHidden = false
        default:
            if Int(p4.seventeen) > 3 {
                p4seventeenNum.text = "\(Int(p4.seventeen) * 17)"
            }
        }
        
        //16
        switch Int(p4.sixteen) {
        case 0:
            break
        case 1:
            p4sixteenLeft.isHidden = false
        case 2:
            p4sixteenRight.isHidden = false
            p4sixteenLeft.isHidden = false
        case 3:
            p4sixteenCenter.isHidden = false
            p4sixteenLeft.isHidden = false
            p4sixteenRight.isHidden = false
        default:
            if Int(p4.sixteen) > 3 {
                p4sixteenNum.text = "\(Int(p4.sixteen) * 16)"
            }
        }
        
        //15
        switch Int(p4.fifteen) {
        case 0:
            break
        case 1:
            p4fifteenLeft.isHidden = false
        case 2:
            p4fifteenRight.isHidden = false
            p4fifteenLeft.isHidden = false
        case 3:
            p4fifteenCenter.isHidden = false
            p4fifteenLeft.isHidden = false
            p4fifteenRight.isHidden = false
        default:
            if Int(p4.fifteen) > 3 {
                p4fifteenNum.text = "\(Int(p4.fifteen) * 15)"
            }
        }
        
        //bull
        switch Int(p4.bull) {
        case 0:
            break
        case 1:
            p4bullLeft.isHidden = false
        case 2:
            p4bullRight.isHidden = false
            p4bullLeft.isHidden = false
        case 3:
            p4bullCenter.isHidden = false
            p4bullLeft.isHidden = false
            p4bullRight.isHidden = false
        default:
            if Int(p4.bull) > 3 {
                p4bullNum.text = "\(Int(p4.bull) * 25)"
            }
        }
        
    }
    
    
    
    //    MARK: Core Data Functions
    func fetch() {
        let request: NSFetchRequest<PlayerInfo> = PlayerInfo.fetchRequest()
        let results = try! managedContext.fetch(request)
        
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        
        gameInfo = gameresults.first!

        
        p1 = results[0]
        p2 = results[1]
        p3 = results[2]
        p4 = results[3]
    }
    
    func saveData() {
        do {
            try managedContext.save()
        } catch {
            print("Could Not Save Data")
        }
    }
    
    
    
}
