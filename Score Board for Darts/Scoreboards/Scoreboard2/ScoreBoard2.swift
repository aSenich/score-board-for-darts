//
//  ScoreBoard2.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/28/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit
import CoreData

class ScoreBoard2: UIViewController {
    
    var managedContext: NSManagedObjectContext!
    
    var p1: PlayerInfo!
    var p2: PlayerInfo!
    var p3: PlayerInfo!
    var p4: PlayerInfo!
    
    var gameInfo: CurrentGameInfo!
    
    var player1TotalScore = 0
    var player2TotalScore = 0
    var player3TotalSocre = 0
    var player4TotalScore = 0

    
    
    // MARK: Actions

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    // MARK: Active Outlets
    @IBOutlet weak var name1Text: UILabel!
    @IBOutlet weak var name2Text: UILabel!
    @IBOutlet weak var Name3Text: UILabel!
    @IBOutlet weak var name4Text: UILabel!
    
    @IBOutlet weak var p1TotalScore: UILabel!
    @IBOutlet weak var p2TotalScore: UILabel!
    @IBOutlet weak var p3TotalScore: UILabel!
    @IBOutlet weak var p4TotalScore: UILabel!
    
    // MARK: Scores marks Outlets
    
    // Player1
    //20
    @IBOutlet weak var p1twentyLeft: UILabel!
    @IBOutlet weak var p1twentyCenter: UILabel!
    @IBOutlet weak var p1twentyRight: UILabel!
    
    @IBOutlet weak var p1twentyNum: UILabel!
    
    //19
    @IBOutlet weak var p1nineteenLeft: UILabel!
    @IBOutlet weak var p1nineteenCenter: UILabel!
    @IBOutlet weak var p1nineteenRight: UILabel!
    
    @IBOutlet weak var p1nineteenNum: UILabel!
    
    //18
    @IBOutlet weak var p1eightteenLeft: UILabel!
    @IBOutlet weak var p1eightteenCenter: UILabel!
    @IBOutlet weak var p1eightteenRight: UILabel!
    
    @IBOutlet weak var p1eightteenNum: UILabel!
    
    //17
    @IBOutlet weak var p1seventeenLeft: UILabel!
    @IBOutlet weak var p1seventeenCenter: UILabel!
    @IBOutlet weak var p1seventeenRight: UILabel!
    
    @IBOutlet weak var p1seventeenNum: UILabel!
    
    //16
    @IBOutlet weak var p1sixteenLeft: UILabel!
    @IBOutlet weak var p1sixteenCenter: UILabel!
    @IBOutlet weak var p1sixteenRight: UILabel!
    
    @IBOutlet weak var p1sixteenNum: UILabel!
    
    //15
    @IBOutlet weak var p1fifteenLeft: UILabel!
    @IBOutlet weak var p1fifteenCenter: UILabel!
    @IBOutlet weak var p1fifteenRight: UILabel!
    
    
    @IBOutlet weak var p1fifteenNum: UILabel!
    //Bull
    @IBOutlet weak var p1bullLeft: UILabel!
    @IBOutlet weak var p1bullCenter: UILabel!
    @IBOutlet weak var p1bullRight: UILabel!
    
    @IBOutlet weak var p1bullNum: UILabel!
    
    
    // Player 2
    //20
    @IBOutlet weak var p2twentyLeft: UILabel!
    @IBOutlet weak var p2twentyCenter: UILabel!
    @IBOutlet weak var p2twentyRight: UILabel!
    
    @IBOutlet weak var p2twentyNum: UILabel!
    
    //19
    @IBOutlet weak var p2nineteenLeft: UILabel!
    @IBOutlet weak var p2nineteenCenter: UILabel!
    @IBOutlet weak var p2nineteenRight: UILabel!
    
    @IBOutlet weak var p2nineteenNum: UILabel!
    
    //18
    @IBOutlet weak var p2eightteenLeft: UILabel!
    @IBOutlet weak var p2eightteenCenter: UILabel!
    @IBOutlet weak var p2eightteenRight: UILabel!
    
    @IBOutlet weak var p2eightteenNum: UILabel!
    
    //17
    @IBOutlet weak var p2seventeenLeft: UILabel!
    @IBOutlet weak var p2seventeenCenter: UILabel!
    @IBOutlet weak var p2seventeenRight: UILabel!
    
    @IBOutlet weak var p2seventeenNum: UILabel!
    
    //16
    @IBOutlet weak var p2sixteenLeft: UILabel!
    @IBOutlet weak var p2sixteenCenter: UILabel!
    @IBOutlet weak var p2sixteenRight: UILabel!
    
    @IBOutlet weak var p2sixteenNum: UILabel!
    
    //15
    @IBOutlet weak var p2fifteenLeft: UILabel!
    @IBOutlet weak var p2fifteenCenter: UILabel!
    @IBOutlet weak var p2fifteenRight: UILabel!
    
    @IBOutlet weak var p2fifteenNum: UILabel!
    
    //bull
    @IBOutlet weak var p2bullLeft: UILabel!
    @IBOutlet weak var p2bullCenter: UILabel!
    @IBOutlet weak var p2bullRight: UILabel!
    
    
    @IBOutlet weak var p2bullNum: UILabel!
    
    // Player 3
    //20
    @IBOutlet weak var p3twentyLeft: UILabel!
    @IBOutlet weak var p3twentyCenter: UILabel!
    @IBOutlet weak var p3twentyRight: UILabel!
    
    @IBOutlet weak var p3twentyNum: UILabel!
    
    //19
    @IBOutlet weak var p3nineteenLeft: UILabel!
    @IBOutlet weak var p3nneteenCenter: UILabel!
    @IBOutlet weak var p3nineteenRight: UILabel!
    
    @IBOutlet weak var p3nineteenNum: UILabel!
    
    //18
    @IBOutlet weak var p3eightteenLeft: UILabel!
    @IBOutlet weak var p3eightteenCenter: UILabel!
    @IBOutlet weak var p3eightteenRight: UILabel!
    
    @IBOutlet weak var p3eightteenNum: UILabel!
    
    //17
    @IBOutlet weak var p3seventeenLeft: UILabel!
    @IBOutlet weak var p3seventeenCenter: UILabel!
    @IBOutlet weak var p3seventeenRight: UILabel!
    
    @IBOutlet weak var p3seventeenNum: UILabel!
    
    
    //16
    @IBOutlet weak var p3sixteenLeft: UILabel!
    @IBOutlet weak var p3sixteenCenter: UILabel!
    @IBOutlet weak var p3sixteenRight: UILabel!
    
    @IBOutlet weak var p3sixteenNum: UILabel!
    
    //15
    @IBOutlet weak var p3fifteenLeft: UILabel!
    @IBOutlet weak var p3fifteenCenter: UILabel!
    @IBOutlet weak var p3fifteenRight: UILabel!
    
    @IBOutlet weak var p3fifteenNum: UILabel!
    
    //bull
    @IBOutlet weak var p3bullLeft: UILabel!
    @IBOutlet weak var p3bullCenter: UILabel!
    @IBOutlet weak var p3bullRight: UILabel!
    
    
    @IBOutlet weak var p3bullNum: UILabel!
    
    // Player 4
    //20
    @IBOutlet weak var p4twentyLeft: UILabel!
    @IBOutlet weak var p4twentyCenter: UILabel!
    @IBOutlet weak var p4twentyRight: UILabel!
    
    @IBOutlet weak var p4twentyNum: UILabel!
    
    //19
    @IBOutlet weak var p4nineteenLeft: UILabel!
    @IBOutlet weak var p4nineteenCenter: UILabel!
    @IBOutlet weak var p4nineteenRight: UILabel!
    
    @IBOutlet weak var p4nineteenNum: UILabel!
    
    //18
    @IBOutlet weak var p4eightteenLeft: UILabel!
    @IBOutlet weak var p4eightteenCenter: UILabel!
    @IBOutlet weak var p4eightteenRight: UILabel!
    
    @IBOutlet weak var p4eightteenNum: UILabel!
    
    //17
    @IBOutlet weak var p4seventeenLeft: UILabel!
    @IBOutlet weak var p4seventeenCenter: UILabel!
    @IBOutlet weak var p4seventeenRight: UILabel!
    
    @IBOutlet weak var p4seventeenNum: UILabel!
    
    //16
    @IBOutlet weak var p4sixteenLeft: UILabel!
    @IBOutlet weak var p4sixteenCenter: UILabel!
    @IBOutlet weak var p4sixteenRight: UILabel!
    
    
    @IBOutlet weak var p4sixteenNum: UILabel!
    //15
    @IBOutlet weak var p4fifteenLeft: UILabel!
    @IBOutlet weak var p4fifteenCenter: UILabel!
    @IBOutlet weak var p4fifteenRight: UILabel!
    
    @IBOutlet weak var p4fifteenNum: UILabel!
    
    //bull
    @IBOutlet weak var p4bullLeft: UILabel!
    @IBOutlet weak var p4bullCenter: UILabel!
    @IBOutlet weak var p4bullRight: UILabel!
    
    @IBOutlet weak var p4bullNum: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        totalScores()
        // Do any additional setup after loading the view.
        
     setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
   setUp2()
        
    }
    

    
  
        }


    
    



