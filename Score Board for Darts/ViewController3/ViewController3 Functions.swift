//
//  ViewController3 Functions.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 2/16/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

extension ViewController3 {
    @objc func refresh() {
        
        toggleBlur(On: false)
        
        editSocresBtn.isEnabled = true
        
        refreshAd()
        
        fetch()
        gameInfo.currentLocation = 4
        gameInfo.notificationWasDisplayed = false
        saveData()
        fetch()
        
        switch gameInfo.numberOfPlayers {
        case 2:
            if gameInfo.currentPlayer == 3 {
                gameInfo.currentPlayer = 1
                
                do {
                    try managedContext.save()
                } catch {
                    print("Could not save data")
                }
                fetch()
            }
        case 3:
            if gameInfo.currentPlayer == 4 {
                gameInfo.currentPlayer = 1
                
                do {
                    try managedContext.save()
                } catch {
                    print("Could not save data")
                }
                fetch()
            }
        case 4:
            if gameInfo.currentPlayer == 5 {
                gameInfo.currentPlayer = 1
                
                do {
                    try managedContext.save()
                } catch {
                    print("Could not save data")
                }
                fetch()
            }
        default:
            break
        }
        
        setUps()
        DispatchQueue.main.async {
            
            //            self.setTextFields()
            
            self.secondNumberDirections.isHidden = true
            
            self.includedNumbersText.alpha = 1
            
            switch self.gameInfo.currentPlayer {
            case 1:
                self.currentPlayerName = self.p1.name
            case 2:
                self.currentPlayerName = self.p2.name
            case 3:
                self.currentPlayerName = self.p3.name
            case 4:
                self.currentPlayerName = self.p4.name
            default:
                break
            }
            
            self.notification.removeFromSuperview()
            self.gameInfo.notificationYesWasPressed = false
            self.saveData()
            self.fetch()
            
            
            self.gameInfo.notificationWasDisplayed = false
            self.saveData()
            self.fetch()
            switch self.gameInfo.numberOfPlayers {
            case 2:
                
                switch self.gameInfo.currentPlayer {
                case 1:
                    self.currentPlayerTitle.text = "It is \(self.p1.name!)'s turn"
                    self.upNextPlayerTitle.text = "\(self.p2.name!) is up next"
                case 2:
                    DispatchQueue.main.async {
                        self.currentPlayerTitle.text = "It is \(self.p2.name!)'s turn"
                        self.upNextPlayerTitle.text = "\(self.p1.name!) is up next"
                    }
                default:
                    print("Error in player order tracker")
                }
            case 3:
                
                switch self.gameInfo.currentPlayer {
                case 1:
                    self.currentPlayerTitle.text = "It is \(self.p1.name!)'s turn"
                    self.upNextPlayerTitle.text = "\(self.p2.name!) is up next"
                case 2:
                    self.currentPlayerTitle.text = "It is \(self.p2.name!)'s turn"
                    self.upNextPlayerTitle.text = "\(self.p3.name!) is up next"
                case 3:
                    self.currentPlayerTitle.text = "It is \(self.p3.name!)'s turn"
                    self.upNextPlayerTitle.text = "\(self.p1.name!) is up next"
                default:
                    print("Error in player order tracker")
                }
                
            case 4:
                
                switch self.gameInfo.currentPlayer {
                case 1:
                    self.currentPlayerTitle.text = "It is \(self.p1.name!)'s turn"
                    self.upNextPlayerTitle.text = "\(self.p2.name!) is up next"
                case 2:
                    self.currentPlayerTitle.text = "It is \(self.p2.name!)'s turn"
                    self.upNextPlayerTitle.text = "\(self.p3.name!) is up next"
                case 3:
                    self.currentPlayerTitle.text = "It is \(self.p3.name!)'s turn"
                    self.upNextPlayerTitle.text = "\(self.p4.name!) is up next"
                case 4:
                    self.currentPlayerTitle.text = "It is \(self.p4.name!)'s turn"
                    self.upNextPlayerTitle.text = "\(self.p1.name!) is up next"
                default:
                    print("Error in player order tracker")
                }
                
            default:
                print("Error in number of players tracker")
            }
        }
    }
    
    
    func noHitsDirections2() {
        UIView.animate(withDuration: 1, animations: {
            self.noHitsDirections.alpha = 1.0
            DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
                UIView.animate(withDuration: 1, animations: {
                    self.noHitsDirections.alpha = 0.0
                })
            }
            
        })
    }
    
    func notificationPopUp() {
        print("Notification was displayed value before appearing = \(gameInfo.notificationWasDisplayed)")
        print("Notification yes button was pressed value before appearing = \(gameInfo.notificationYesWasPressed)")
        self.view.addSubview(notification)
        notification.center = self.view.center
        
        
        notification.alpha = 0.0
        
        gameInfo.notificationWasDisplayed = true
        saveData()
        fetch()
        print("Pop up was diplayed = \(gameInfo.notificationWasDisplayed)")
        UIView.animate(withDuration: 0.3, animations: {
            
            self.notification.alpha = 1.0
        })
        print("Notification was displayed value after appearing = \(gameInfo.notificationWasDisplayed)")
        print("Notification yes button was pressed value after appearing = \(gameInfo.notificationYesWasPressed)")
    }
    
    
    func refreshAd() {
        //Ad
        self.view.addSubview(adBanner)
        
        
        adBanner.center.x = self.view.center.x
        adBanner.frame.origin.y = self.view.frame.height - adBanner.frame.height
        
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
    }
    
    
    func toggleBlur(On: Bool) {
        if On == true {
            self.view.addSubview(blur)
            blur.center = self.view.center
            
            blur.alpha = 0
            
            UIView.animate(withDuration: 0.2) {
                self.blur.alpha = 1
            }
        } else {
            
            
            UIView.animate(withDuration: 0.2, animations: {
                self.blur.alpha = 0.0
            }) { (success:Bool) in
                self.blur.removeFromSuperview()
            }
            
            
        }
    }
    
    func scoreEditPopUp() {
        setUps()
        totalTempScores()
        
        self.view.addSubview(socresEditView)
        socresEditView.center = self.view.center
        
        socresEditView.alpha = 0
        
        UIView.animate(withDuration: 0.0) {
            self.socresEditView.alpha = 1
        }
    }
    
    func goingTo2() {
        switch goingTo {
        case 20:
            performSegue(withIdentifier: "20", sender: self)
        case 18:
            performSegue(withIdentifier: "18", sender: self)
        case 15:
            performSegue(withIdentifier: "15", sender: self)
        case 17:
            performSegue(withIdentifier: "17", sender: self)
        case 19:
            performSegue(withIdentifier: "19", sender: self)
        case 16:
            performSegue(withIdentifier: "16", sender: self)
        case 25:
            performSegue(withIdentifier: "bull", sender: self)
        default:
            break
        }
    }
    
    @objc func dismissPop() {
        self.notification.removeFromSuperview()
        
        toggleBlur(On: false)
        switch gameInfo.currentPlayer {
        case 1:
            self.currentPlayerName = p1.name!
        case 2:
            self.currentPlayerName = p2.name!
        case 3:
            self.currentPlayerName = p3.name!
        case 4:
            self.currentPlayerName = p4.name!
        default:
            break
        }
        
        secondNumberDirections.isHidden = false
        secondNumberDirections.text = "\(currentPlayerName!), please select your other number or press the No Hits button to skip to the next player"
        
        editSocresBtn.isEnabled = false
        
        extraTwo.alpha = 0
        extraThree.alpha = 0
        notYes.alpha = 1
        notNo.alpha = 1
        notText.text = "Did you hit more than one number that counts in cricket?"
        includedNumbersText.alpha = 0
        print("Notification value after dismissing it = \(gameInfo.notificationWasDisplayed)")
    }
    
    func bringAllToFront() {
        self.view.bringSubviewToFront(self.twentyBtn)
        self.view.bringSubviewToFront(self.oneBtn)
        self.view.bringSubviewToFront(self.eightteenBtn)
        self.view.bringSubviewToFront(self.fouBtn)
        self.view.bringSubviewToFront(self.thirteenBtn)
        self.view.bringSubviewToFront(self.sixBtn)
        self.view.bringSubviewToFront(self.tenBtn)
        self.view.bringSubviewToFront(self.fifteenBtn)
        self.view.bringSubviewToFront(self.twoBtn)
        self.view.bringSubviewToFront(self.seventeenBtn)
        self.view.bringSubviewToFront(self.threeBtn)
        self.view.bringSubviewToFront(self.nineteenBtn)
        self.view.bringSubviewToFront(self.sevenBtn)
        self.view.bringSubviewToFront(self.sixteenBtn)
        self.view.bringSubviewToFront(self.eightBtn)
        self.view.bringSubviewToFront(self.elevenBtn)
        self.view.bringSubviewToFront(self.fourteenBtn)
        self.view.bringSubviewToFront(self.nineBtn)
        self.view.bringSubviewToFront(self.twelveBtn)
        self.view.bringSubviewToFront(self.fiveBtn)
        self.view.bringSubviewToFront(self.bullBtn)
        self.view.bringSubviewToFront(self.toScoreboardBtn)
        self.view.bringSubviewToFront(self.back)
        self.view.bringSubviewToFront(self.noHitsBtn)
        self.view.bringSubviewToFront(self.returnPlayer)
    }
    
    func hideAllBtns() {
        twentyBtn.isHidden = true
        oneBtn.isHidden = true
        eightteenBtn.isHidden = true
        fouBtn.isHidden = true
        thirteenBtn.isHidden = true
        sixBtn.isHidden = true
        tenBtn.isHidden = true
        fifteenBtn.isHidden = true
        twoBtn.isHidden = true
        seventeenBtn.isHidden = true
        threeBtn.isHidden = true
        nineteenBtn.isHidden = true
        sevenBtn.isHidden = true
        sixteenBtn.isHidden = true
        eightBtn.isHidden = true
        elevenBtn.isHidden = true
        fourteenBtn.isHidden = true
        nineBtn.isHidden = true
        twelveBtn.isHidden = true
        fiveBtn.isHidden = true
        noHitsBtn.isHidden = true
        returnPlayer.isHidden = true
        back.isHidden = true
    }
    
    func unhideAllBtns() {
        twentyBtn.isHidden = false
        oneBtn.isHidden = false
        eightteenBtn.isHidden = false
        fouBtn.isHidden = false
        thirteenBtn.isHidden = false
        sixBtn.isHidden = false
        tenBtn.isHidden = false
        fifteenBtn.isHidden = false
        twoBtn.isHidden = false
        seventeenBtn.isHidden = false
        threeBtn.isHidden = false
        nineteenBtn.isHidden = false
        sevenBtn.isHidden = false
        sixteenBtn.isHidden = false
        eightBtn.isHidden = false
        elevenBtn.isHidden = false
        fourteenBtn.isHidden = false
        nineBtn.isHidden = false
        twelveBtn.isHidden = false
        fiveBtn.isHidden = false
        noHitsBtn.isHidden = false
        returnPlayer.isHidden = false
        back.isHidden = false
    }
    
    
    
    func setNumbers() {
        switch Int(gameInfo.numberOfPlayers) {
        case 2:
            DispatchQueue.main.async {
                self.p1twentyLablel.text = String(self.p1.twenety)
                self.p1nineteenLabel.text = String(self.p1.nineteen)
                self.p1eightteenLabel.text = String(self.p1.eighteen)
                self.p1seventeenLabel.text = String(self.p1.seventeen)
                self.p1sixteenLabel.text = String(self.p1.sixteen)
                self.p1fifteenLabel.text = String(self.p1.fifteen)
                self.p1bullLabel.text = String(self.p1.bull)
                
                self.p2twentyLabel.text = String(self.p2.twenety)
                self.p2nineteenLabel.text = String(self.p2.nineteen)
                self.p2eightteenLabel.text = String(self.p2.eighteen)
                self.p2seveneteenLabel.text = String(self.p2.seventeen)
                self.p2sixteenLabel.text = String(self.p2.sixteen)
                self.p2fifteenLabel.text = String(self.p2.fifteen)
                self.p2bullLabel.text = String(self.p2.bull)
            }
            p1twentyStep.value = Double(p1.twenety)
            p1nineteenStep.value = Double(p1.nineteen)
            p1eightteenStep.value = Double(p1.eighteen)
            p1seventeenStep.value = Double(p1.seventeen)
            p1sixteenStep.value = Double(p1.sixteen)
            p1fifteenStep.value = Double(p1.fifteen)
            p1bullStep.value = Double(p1.bull)
            
            p2twentyStep.value = Double(p2.twenety)
            p2nineteenStep.value = Double(p2.nineteen)
            p2eightteenStep.value = Double(p2.eighteen)
            p2seventeenStep.value = Double(p2.seventeen)
            p2sixteenStep.value = Double(p2.sixteen)
            p2fifteenStep.value = Double(p2.fifteen)
            p2bullStep.value = Double(p2.bull)
        case 3:
            DispatchQueue.main.async {
                self.p1twentyLablel.text = String(self.p1.twenety)
                self.p1nineteenLabel.text = String(self.p1.nineteen)
                self.p1eightteenLabel.text = String(self.p1.eighteen)
                self.p1seventeenLabel.text = String(self.p1.seventeen)
                self.p1sixteenLabel.text = String(self.p1.sixteen)
                self.p1fifteenLabel.text = String(self.p1.fifteen)
                self.p1bullLabel.text = String(self.p1.bull)
                
                self.p2twentyLabel.text = String(self.p2.twenety)
                self.p2nineteenLabel.text = String(self.p2.nineteen)
                self.p2eightteenLabel.text = String(self.p2.eighteen)
                self.p2seveneteenLabel.text = String(self.p2.seventeen)
                self.p2sixteenLabel.text = String(self.p2.sixteen)
                self.p2fifteenLabel.text = String(self.p2.fifteen)
                self.p2bullLabel.text = String(self.p2.bull)
                
                self.p3twentyLabel.text = String(self.p3.twenety)
                self.p3nineteenLabel.text = String(self.p3.nineteen)
                self.p3eightteenLabel.text = String(self.p3.eighteen)
                self.p3seveneteenLabel.text = String(self.p3.seventeen)
                self.p3sixteenLabel.text = String(self.p3.sixteen)
                self.p3fifteenLabel.text = String(self.p3.fifteen)
                self.p3bullLabel.text = String(self.p3.bull)
            }
            p1twentyStep.value = Double(p1.twenety)
            p1nineteenStep.value = Double(p1.nineteen)
            p1eightteenStep.value = Double(p1.eighteen)
            p1seventeenStep.value = Double(p1.seventeen)
            p1sixteenStep.value = Double(p1.sixteen)
            p1fifteenStep.value = Double(p1.fifteen)
            p1bullStep.value = Double(p1.bull)
            
            p2twentyStep.value = Double(p2.twenety)
            p2nineteenStep.value = Double(p2.nineteen)
            p2eightteenStep.value = Double(p2.eighteen)
            p2seventeenStep.value = Double(p2.seventeen)
            p2sixteenStep.value = Double(p2.sixteen)
            p2fifteenStep.value = Double(p2.fifteen)
            p2bullStep.value = Double(p2.bull)
            
            p3TwentyStep.value = Double(p3.twenety)
            p3nineteenStep.value = Double(p3.nineteen)
            p3eightteenStep.value = Double(p3.eighteen)
            p3seventeenStep.value = Double(p3.seventeen)
            p3sixteenStep.value = Double(p3.sixteen)
            p3fifteenStep.value = Double(p3.fifteen)
            p3bullStep.value = Double(p3.bull)
        case 4:
            DispatchQueue.main.async {
                self.p1twentyLablel.text = String(self.p1.twenety)
                self.p1nineteenLabel.text = String(self.p1.nineteen)
                self.p1eightteenLabel.text = String(self.p1.eighteen)
                self.p1seventeenLabel.text = String(self.p1.seventeen)
                self.p1sixteenLabel.text = String(self.p1.sixteen)
                self.p1fifteenLabel.text = String(self.p1.fifteen)
                self.p1bullLabel.text = String(self.p1.bull)
                
                self.p2twentyLabel.text = String(self.p2.twenety)
                self.p2nineteenLabel.text = String(self.p2.nineteen)
                self.p2eightteenLabel.text = String(self.p2.eighteen)
                self.p2seveneteenLabel.text = String(self.p2.seventeen)
                self.p2sixteenLabel.text = String(self.p2.sixteen)
                self.p2fifteenLabel.text = String(self.p2.fifteen)
                self.p2bullLabel.text = String(self.p2.bull)
                
                self.p3twentyLabel.text = String(self.p3.twenety)
                self.p3nineteenLabel.text = String(self.p3.nineteen)
                self.p3eightteenLabel.text = String(self.p3.eighteen)
                self.p3seveneteenLabel.text = String(self.p3.seventeen)
                self.p3sixteenLabel.text = String(self.p3.sixteen)
                self.p3fifteenLabel.text = String(self.p3.fifteen)
                self.p3bullLabel.text = String(self.p3.bull)
                
                self.p4twentyLabel.text = String(self.p4.twenety)
                self.p4ninteenLabel.text = String(self.p4.nineteen)
                self.p4eightteenLabel.text = String(self.p4.eighteen)
                self.p4seventeenLabel.text = String(self.p4.seventeen)
                self.p4sixteenLabel.text = String(self.p4.sixteen)
                self.p4fifteenLabel.text = String(self.p4.fifteen)
                self.p4bullLabel.text = String(self.p4.bull)
            }
            p1twentyStep.value = Double(p1.twenety)
            p1nineteenStep.value = Double(p1.nineteen)
            p1eightteenStep.value = Double(p1.eighteen)
            p1seventeenStep.value = Double(p1.seventeen)
            p1sixteenStep.value = Double(p1.sixteen)
            p1fifteenStep.value = Double(p1.fifteen)
            p1bullStep.value = Double(p1.bull)
            
            p2twentyStep.value = Double(p2.twenety)
            p2nineteenStep.value = Double(p2.nineteen)
            p2eightteenStep.value = Double(p2.eighteen)
            p2seventeenStep.value = Double(p2.seventeen)
            p2sixteenStep.value = Double(p2.sixteen)
            p2fifteenStep.value = Double(p2.fifteen)
            p2bullStep.value = Double(p2.bull)
            
            p3TwentyStep.value = Double(p3.twenety)
            p3nineteenStep.value = Double(p3.nineteen)
            p3eightteenStep.value = Double(p3.eighteen)
            p3seventeenStep.value = Double(p3.seventeen)
            p3sixteenStep.value = Double(p3.sixteen)
            p3fifteenStep.value = Double(p3.fifteen)
            p3bullStep.value = Double(p3.bull)
            
            p4twentyStep.value = Double(p4.twenety)
            p4nineteenStep.value = Double(p4.nineteen)
            p4eightteenStep.value = Double(p4.eighteen)
            p4seventeenStep.value = Double(p4.seventeen)
            p4sixteenStep.value = Double(p4.sixteen)
            p4fifteenStep.value = Double(p4.fifteen)
            p4bullStep.value = Double(p4.bull)
        default:
            break
        }
        
    }
    
    
    
    
    func setUps() {
        setNumbers()
    }
    
    func changeNumberText(NumberLabel: UILabel!, CurrentValue: Int, stepper: UIStepper) {
        NumberLabel.text = String(CurrentValue)
        
        
        totalTempScores()
    }
    
    
    
    
    
    func enableUpdateBtn() {
        
        updateBtn.isEnabled = true
    }
    
    
    
    func totalTempScores() {
        
        switch Int(gameInfo.numberOfPlayers) {
        case 2:
            let p1scorePart1 = (Int(p1.twenety) * 20) + (Int(p1.nineteen) * 19) + (Int(p1.eighteen) * 18)
            let p1scorePart2 = (Int(p1.seventeen) * 17) + (Int(p1.sixteen) * 16) + (Int(p1.fifteen) * 15) + (Int(p1.bull) * 25)
            let p1score = p1scorePart1 + p1scorePart2
            
            let p2scorePart1 = (Int(p2.twenety) * 20) + (Int(p2.nineteen) * 19) + (Int(p2.eighteen) * 18)
            let p2scorePart2 = (Int(p2.seventeen) * 17) + (Int(p2.sixteen) * 16) + (Int(p2.fifteen) * 15) + (Int(p2.bull) * 25)
            let p2score = p2scorePart1 + p2scorePart2
            
            p1TempScore.text = String(p1score)
            p2TempScore.text = String(p2score)
            p3TempScore.text = "0"
            p4TempScore.text = "0"
        case 3:
            let p1scorePart1 = (Int(p1.twenety) * 20) + (Int(p1.nineteen) * 19) + (Int(p1.eighteen) * 18)
            let p1scorePart2 = (Int(p1.seventeen) * 17) + (Int(p1.sixteen) * 16) + (Int(p1.fifteen) * 15) + (Int(p1.bull) * 25)
            let p1score = p1scorePart1 + p1scorePart2
            
            let p2scorePart1 = (Int(p2.twenety) * 20) + (Int(p2.nineteen) * 19) + (Int(p2.eighteen) * 18)
            let p2scorePart2 = (Int(p2.seventeen) * 17) + (Int(p2.sixteen) * 16) + (Int(p2.fifteen) * 15) + (Int(p2.bull) * 25)
            let p2score = p2scorePart1 + p2scorePart2
            
            let p3scorePart1 = (Int(p3.twenety) * 20) + (Int(p3.nineteen) * 19) + (Int(p3.eighteen) * 18)
            let p3scorePart2 = (Int(p3.seventeen) * 17) + (Int(p3.sixteen) * 16) + (Int(p3.fifteen) * 15) + (Int(p3.bull) * 25)
            let p3score = p3scorePart1 + p3scorePart2
            
            p1TempScore.text = String(p1score)
            p2TempScore.text = String(p2score)
            p3TempScore.text = String(p3score)
            p4TempScore.text = "0"
        case 4:
            let p1scorePart1 = (Int(p1.twenety) * 20) + (Int(p1.nineteen) * 19) + (Int(p1.eighteen) * 18)
            let p1scorePart2 = (Int(p1.seventeen) * 17) + (Int(p1.sixteen) * 16) + (Int(p1.fifteen) * 15) + (Int(p1.bull) * 25)
            let p1score = p1scorePart1 + p1scorePart2
            
            let p2scorePart1 = (Int(p2.twenety) * 20) + (Int(p2.nineteen) * 19) + (Int(p2.eighteen) * 18)
            let p2scorePart2 = (Int(p2.seventeen) * 17) + (Int(p2.sixteen) * 16) + (Int(p2.fifteen) * 15) + (Int(p2.bull) * 25)
            let p2score = p2scorePart1 + p2scorePart2
            
            let p3scorePart1 = (Int(p3.twenety) * 20) + (Int(p3.nineteen) * 19) + (Int(p3.eighteen) * 18)
            let p3scorePart2 = (Int(p3.seventeen) * 17) + (Int(p3.sixteen) * 16) + (Int(p3.fifteen) * 15) + (Int(p3.bull) * 25)
            let p3score = p3scorePart1 + p3scorePart2
            
            let p4scorePart1 = (Int(p4.twenety) * 20) + (Int(p4.nineteen) * 19) + (Int(p4.eighteen) * 18)
            let p4scorePart2 = (Int(p4.seventeen) * 17) + (Int(p4.sixteen) * 16) + (Int(p4.fifteen) * 15) + (Int(p4.bull) * 25)
            let p4score = p4scorePart1 + p4scorePart2
            
            
            p1TempScore.text = String(p1score)
            p2TempScore.text = String(p2score)
            p3TempScore.text = String(p3score)
            p4TempScore.text = String(p4score)
        default:
            break
        }
        
        
    }
    
    @objc func hideUndoButton() {
        returnPlayer.isHidden = true
    }
    
    func fetch() {
        let request: NSFetchRequest<PlayerInfo> = PlayerInfo.fetchRequest()
        let results = try! managedContext.fetch(request)
        
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        
        gameInfo = gameresults.first!
        
        
        p1 = results[0]
        p2 = results[1]
        p3 = results[2]
        p4 = results[3]
    }
    func saveData() {
        do {
            try managedContext.save()
        } catch {
            print("Could Not Save Data")
        }
    }
    
    
    
}
