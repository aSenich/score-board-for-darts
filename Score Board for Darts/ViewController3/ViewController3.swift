//
//  ViewController3.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/9/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class ViewController3: UIViewController, UITextFieldDelegate {
    
    var managedContext: NSManagedObjectContext!
    
    var p1: PlayerInfo!
    var p2: PlayerInfo!
    var p3: PlayerInfo!
    var p4: PlayerInfo!
    
    var gameInfo: CurrentGameInfo!
    
    
    // MARK: Vars
    var timesHit = 0
    var goingTo = 0
    var currentPlayerName: String!
    var rusumingGame = false
    
    
    
    
    
    @IBAction func unwindToDartboard(segue:UIStoryboardSegue) {}
    
    // MARK: Outlets
    @IBOutlet weak var currentPlayerTitle: UILabel!
    @IBOutlet weak var upNextPlayerTitle: UILabel!
    @IBOutlet weak var returnPlayer: UIButton!
    @IBOutlet weak var alert1: UILabel!
    @IBOutlet weak var alert2: UILabel!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var noHitsDirections: UILabel!
    @IBOutlet var notification: UIView!
    @IBOutlet weak var notYes: UIButton!
    @IBOutlet weak var notNo: UIButton!
    @IBOutlet weak var notText: UILabel!
    @IBOutlet weak var extraTwo: UIButton!
    @IBOutlet weak var extraThree: UIButton!
    @IBOutlet weak var secondNumberDirections: UILabel!
    @IBOutlet weak var includedNumbersText: UILabel!
    @IBOutlet weak var noHitsBtn: UIButton!
    @IBOutlet weak var editSocresBtn: UIButton!
    @IBOutlet weak var adBanner: GADBannerView!
    @IBOutlet var blur: UIVisualEffectView!
    
    
    // MARK: Scores Edit View Outlets
    @IBOutlet var socresEditView: UIView!
    @IBOutlet weak var player1Ename: UILabel!
    @IBOutlet weak var player2Ename: UILabel!
    @IBOutlet weak var player3Ename: UILabel!
    @IBOutlet weak var player4Ename: UILabel!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var scoresEditBackButton: UIButton!
    
    
    // MARK: Steppers and labels
    
//    Player temperary score for the score edit screen
    @IBOutlet weak var p1TempScore: UILabel!
    @IBOutlet weak var p2TempScore: UILabel!
    @IBOutlet weak var p3TempScore: UILabel!
    @IBOutlet weak var p4TempScore: UILabel!
    
    
    
    //Player1
    @IBOutlet weak var p1twentyStep: UIStepper!
    @IBOutlet weak var p1twentyLablel: UILabel!
    
    @IBOutlet weak var p1nineteenStep: UIStepper!
    @IBOutlet weak var p1nineteenLabel: UILabel!
    
    @IBOutlet weak var p1eightteenStep: UIStepper!
    @IBOutlet weak var p1eightteenLabel: UILabel!
    
    
    @IBOutlet weak var p1seventeenStep: UIStepper!
    @IBOutlet weak var p1seventeenLabel: UILabel!
    
    @IBOutlet weak var p1sixteenStep: UIStepper!
    @IBOutlet weak var p1sixteenLabel: UILabel!
    
    
    @IBOutlet weak var p1fifteenStep: UIStepper!
    @IBOutlet weak var p1fifteenLabel: UILabel!
    
    
    @IBOutlet weak var p1bullStep: UIStepper!
    @IBOutlet weak var p1bullLabel: UILabel!
    
    //Player2
    @IBOutlet weak var p2twentyStep: UIStepper!
    @IBOutlet weak var p2twentyLabel: UILabel!
    
    @IBOutlet weak var p2nineteenStep: UIStepper!
    @IBOutlet weak var p2nineteenLabel: UILabel!
    
    @IBOutlet weak var p2eightteenStep: UIStepper!
    @IBOutlet weak var p2eightteenLabel: UILabel!
    
    @IBOutlet weak var p2seventeenStep: UIStepper!
    @IBOutlet weak var p2seveneteenLabel: UILabel!
    
    @IBOutlet weak var p2sixteenStep: UIStepper!
    @IBOutlet weak var p2sixteenLabel: UILabel!
    
    @IBOutlet weak var p2fifteenStep: UIStepper!
    @IBOutlet weak var p2fifteenLabel: UILabel!
    
    @IBOutlet weak var p2bullStep: UIStepper!
    @IBOutlet weak var p2bullLabel: UILabel!
    
    //Player3
    @IBOutlet weak var p3TwentyStep: UIStepper!
    @IBOutlet weak var p3twentyLabel: UILabel!
    
    @IBOutlet weak var p3nineteenStep: UIStepper!
    @IBOutlet weak var p3nineteenLabel: UILabel!
    
    @IBOutlet weak var p3eightteenStep: UIStepper!
    @IBOutlet weak var p3eightteenLabel: UILabel!
    
    @IBOutlet weak var p3seventeenStep: UIStepper!
    @IBOutlet weak var p3seveneteenLabel: UILabel!
    
    @IBOutlet weak var p3sixteenStep: UIStepper!
    @IBOutlet weak var p3sixteenLabel: UILabel!
    
    
    @IBOutlet weak var p3fifteenStep: UIStepper!
    @IBOutlet weak var p3fifteenLabel: UILabel!
    
    @IBOutlet weak var p3bullStep: UIStepper!
    @IBOutlet weak var p3bullLabel: UILabel!
    
    //Player4
    @IBOutlet weak var p4twentyStep: UIStepper!
    @IBOutlet weak var p4twentyLabel: UILabel!
    
    @IBOutlet weak var p4nineteenStep: UIStepper!
    @IBOutlet weak var p4ninteenLabel: UILabel!
    
    @IBOutlet weak var p4eightteenStep: UIStepper!
    @IBOutlet weak var p4eightteenLabel: UILabel!
    
    @IBOutlet weak var p4seventeenStep: UIStepper!
    @IBOutlet weak var p4seventeenLabel: UILabel!
    
    @IBOutlet weak var p4sixteenStep: UIStepper!
    @IBOutlet weak var p4sixteenLabel: UILabel!
    
    @IBOutlet weak var p4fifteenStep: UIStepper!
    @IBOutlet weak var p4fifteenLabel: UILabel!
    
    @IBOutlet weak var p4bullStep: UIStepper!
    @IBOutlet weak var p4bullLabel: UILabel!
    
    
    // MARK: Score Edit Actions for changing player's scores manually
    
    @IBAction func editScoresBtn(_ sender: Any) {
        toggleBlur(On: true)
        scoreEditPopUp()
    }
    
    
    
    @IBAction func updateBtn(_ sender: Any) {
        updateBtn.isEnabled = false
        do {
        try managedContext.save()
        } catch {
            print("Could not save data")
        }
        fetch()
        setUps()
        toggleBlur(On: false)
        UIView.animate(withDuration: 0.5, animations: {
            self.socresEditView.alpha = 0.0
        }) { (success:Bool) in
            self.socresEditView.removeFromSuperview()
        }
        
    }
    @IBAction func backBtnEdit(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.socresEditView.alpha = 0.0
        }) { (success:Bool) in
            self.socresEditView.removeFromSuperview()
    }
        
        toggleBlur(On: false)
        
        p1TempScore.text = nil
        p2TempScore.text = nil
        p3TempScore.text = nil
        p4TempScore.text = nil
        }
    
    // Steppers
    @IBAction func p120(_ sender: UIStepper!) {
        p1.twenety = Int16(Int(p1twentyStep.value))
        p1twentyLablel.text = String(p1.twenety)
        enableUpdateBtn()
    }
    @IBAction func p119(_ sender: UIStepper!) {
        p1.nineteen = Int16(Int(p1nineteenStep.value))
        p1nineteenLabel.text = String(p1.nineteen)
        enableUpdateBtn()
    }
    @IBAction func p118(_ sender: UIStepper!) {
        p1.eighteen = Int16(Int(p1eightteenStep.value))
        p1eightteenLabel.text = String(p1.eighteen)
        enableUpdateBtn()
    }
    @IBAction func p117(_ sender: UIStepper!) {
        p1.seventeen = Int16(Int(p1seventeenStep.value))
        p1seventeenLabel.text = String(p1.seventeen)
        enableUpdateBtn()
    }
    @IBAction func p116(_ sender: UIStepper!) {
        p1.sixteen = Int16(Int(p1sixteenStep.value))
        p1sixteenLabel.text = String(p1.sixteen)
        enableUpdateBtn()
    }
    @IBAction func p115(_ sender: UIStepper!) {
        p1.fifteen = Int16(Int(p1fifteenStep.value))
        p1fifteenLabel.text = String(p1.fifteen)
        enableUpdateBtn()
    }
    @IBAction func p1b(_ sender: UIStepper!) {
        p1.bull = Int16(Int(p1bullStep.value))
        p1bullLabel.text = String(p1.bull)
        enableUpdateBtn()
    }
    
    @IBAction func p220(_ sender: UIStepper!) {
        p2.twenety = Int16(Int(p2twentyStep.value))
        p2twentyLabel.text = String(p2.twenety)
        enableUpdateBtn()
    }
    @IBAction func p219(_ sender: UIStepper!) {
        p2.nineteen = Int16(Int(p2nineteenStep.value))
        p2nineteenLabel.text = String(p2.nineteen)
        enableUpdateBtn()
    }
    @IBAction func p218(_ sender: UIStepper!) {
        p2.eighteen = Int16(Int(p2eightteenStep.value))
        p2eightteenLabel.text = String(p2.eighteen)
        enableUpdateBtn()
    }
    @IBAction func p217(_ sender: UIStepper!) {
        p2.seventeen = Int16(Int(p2seventeenStep.value))
        p2seveneteenLabel.text = String(p2.seventeen)
        enableUpdateBtn()
    }
    @IBAction func p216(_ sender: UIStepper!) {
        p2.sixteen = Int16(Int(p2sixteenStep.value))
        p2sixteenLabel.text = String(p2.sixteen)
        enableUpdateBtn()
    }
    @IBAction func p215(_ sender: UIStepper!) {
        p2.fifteen = Int16(Int(p2fifteenStep.value))
        p2fifteenLabel.text = String(p2.fifteen)
        enableUpdateBtn()
    }
    @IBAction func p2b(_ sender: UIStepper!) {
        p2.bull = Int16(Int(p2bullStep.value))
        p2bullLabel.text = String(p2.bull)
        enableUpdateBtn()
    }
    
    @IBAction func p320(_ sender: UIStepper!) {
        p3.twenety = Int16(Int(p3TwentyStep.value))
        p3twentyLabel.text = String(p3.twenety)
        enableUpdateBtn()
    }
    @IBAction func p319(_ sender: UIStepper!) {
        p3.nineteen = Int16(Int(p3nineteenStep.value))
        p3nineteenLabel.text = String(p3.nineteen)
        enableUpdateBtn()
    }
    @IBAction func p318(_ sender: UIStepper!) {
        p3.eighteen = Int16(Int(p3eightteenStep.value))
        p3eightteenLabel.text = String(p3.eighteen)
        enableUpdateBtn()
    }
    @IBAction func p317(_ sender: UIStepper!) {
        p3.seventeen = Int16(Int(p3seventeenStep.value))
        p3seveneteenLabel.text = String(p3.seventeen)
        enableUpdateBtn()
    }
    @IBAction func p316(_ sender: UIStepper!) {
        p3.sixteen = Int16(Int(p3sixteenStep.value))
        p3sixteenLabel.text = String(p3.sixteen)
        enableUpdateBtn()
    }
    @IBAction func p315(_ sender: UIStepper!) {
        p3.fifteen = Int16(Int(p3fifteenStep.value))
        p3fifteenLabel.text = String(p3.fifteen)
        enableUpdateBtn()
    }
    @IBAction func p3b(_ sender: UIStepper!) {
        p3.bull = Int16(Int(p3bullStep.value))
        p3bullLabel.text = String(p3.bull)
        enableUpdateBtn()
    }
    
    @IBAction func p420(_ sender: UIStepper!) {
        p4.twenety = Int16(Int(p4twentyStep.value))
        p4twentyLabel.text = String(p4.twenety)
        enableUpdateBtn()
    }
    @IBAction func p419(_ sender: UIStepper!) {
        p4.nineteen = Int16(Int(p4nineteenStep.value))
        p4ninteenLabel.text = String(p4.nineteen)
        enableUpdateBtn()
    }
    @IBAction func p418(_ sender: UIStepper!) {
        p4.eighteen = Int16(Int(p4eightteenStep.value))
        p4eightteenLabel.text = String(p4.eighteen)
        enableUpdateBtn()
    }
    @IBAction func p417(_ sender: UIStepper!) {
        p4.seventeen = Int16(Int(p4seventeenStep.value))
        p4seventeenLabel.text = String(p4.seventeen)
        enableUpdateBtn()
    }
    @IBAction func p416(_ sender: UIStepper!) {
        p4.sixteen = Int16(Int(p4sixteenStep.value))
        p4sixteenLabel.text = String(p4.sixteen)
        enableUpdateBtn()
    }
    @IBAction func p415(_ sender: UIStepper!) {
        p4.fifteen = Int16(Int(p4fifteenStep.value))
        p4fifteenLabel.text = String(p4.fifteen)
        enableUpdateBtn()
    }
    @IBAction func p4b(_ sender: UIStepper!) {
        p4.bull = Int16(Int(p4bullStep.value))
        p4bullLabel.text = String(p4.bull)
        enableUpdateBtn()
    }
    
    
    
//    --------------------------------------------------__------------__----_--_--__--_--
    
    // MARK: Button Outlets for dart baord
    @IBOutlet weak var twentyBtn: UIButton!
    @IBOutlet weak var oneBtn: UIButton!
    @IBOutlet weak var eightteenBtn: UIButton!
    @IBOutlet weak var fouBtn: UIButton!
    @IBOutlet weak var thirteenBtn: UIButton!
    @IBOutlet weak var sixBtn: UIButton!
    @IBOutlet weak var tenBtn: UIButton!
    @IBOutlet weak var fifteenBtn: UIButton!
    @IBOutlet weak var twoBtn: UIButton!
    @IBOutlet weak var seventeenBtn: UIButton!
    @IBOutlet weak var threeBtn: UIButton!
    @IBOutlet weak var nineteenBtn: UIButton!
    @IBOutlet weak var sevenBtn: UIButton!
    @IBOutlet weak var sixteenBtn: UIButton!
    @IBOutlet weak var eightBtn: UIButton!
    @IBOutlet weak var elevenBtn: UIButton!
    @IBOutlet weak var fourteenBtn: UIButton!
    @IBOutlet weak var nineBtn: UIButton!
    @IBOutlet weak var twelveBtn: UIButton!
    @IBOutlet weak var fiveBtn: UIButton!
    @IBOutlet weak var bullBtn: UIButton!
    @IBOutlet weak var toScoreboardBtn: UIButton!
    
    
    
    
    
    // MARK: Action Buttons for dart board
    @IBAction func five(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func twelve(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func nine(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func fourteen(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func eleven(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func eight(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func seven(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func three(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func two(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func ten(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func six(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func thirteen(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func four(_ sender: Any) {
        noHitsDirections2()
    }
    @IBAction func one(_ sender: Any) {
        noHitsDirections2()
    }
    
    // MARK: Buttons for notification
    @IBAction func notBack(_ sender: Any) {
        toggleBlur(On: false)
        
        
        if gameInfo.notificationYesWasPressed == false {
        gameInfo.notificationWasDisplayed = false
        UIView.animate(withDuration: 0.5, animations: {
            self.notification.alpha = 0.0
        }) { (success:Bool) in
            self.notification.removeFromSuperview()
        }
            
        } else {
            UIView.animate(withDuration: 0.3) {
                self.extraTwo.alpha = 0
                self.extraThree.alpha = 0
                self.notYes.alpha = 1
                self.notNo.alpha = 1
                self.includedNumbersText.alpha = 1
            }
            notText.text = "Did you hit more than one number that counts in cricket?"
            gameInfo.notificationYesWasPressed = false
            saveData()
            fetch()
        }
    }
    
    @IBAction func notYes(_ sender: Any) {
        gameInfo.notificationYesWasPressed = true
        saveData()
        fetch()
        UIView.animate(withDuration: 0.3) {
            self.extraTwo.alpha = 1
            self.extraThree.alpha = 1
            self.notYes.alpha = 0
            self.notNo.alpha = 0
            self.includedNumbersText.alpha = 0
        }
        notText.text = "How many numbers that count in cricket did you hit?"
    }
    
    @IBAction func popTwo(_ sender: Any) {
        gameInfo.numberOfNumsHit = 2
        
        do {
            try managedContext.save()
        } catch {
            print("Could not save data")
        }
        fetch()
        
        goingTo2()
    }
    @IBAction func popThree(_ sender: Any) {
        gameInfo.numberOfNumsHit = 3
        
        do {
            try managedContext.save()
        } catch {
            print("Could not save data")
        }
        fetch()
        
        goingTo2()
    }
    @IBAction func notNo(_ sender: Any) {
        gameInfo.numberOfNumsHit = 1
        
        do {
            try managedContext.save()
        } catch {
            print("Could not save data")
        }
        fetch()
 
        goingTo2()
    }
    
    
    // MARK: Number buttons on dart board
    
    @IBAction func twenty(_ sender: Any) {
        fetch()
        print("Notification displayed \(gameInfo.notificationWasDisplayed)")

        if gameInfo.notificationWasDisplayed != true {
        goingTo = 20
            toggleBlur(On: true)
        notificationPopUp()
        } else {
            performSegue(withIdentifier: "20", sender: self)
        }
    }
    
    @IBAction func eightteen(_ sender: Any) {
        fetch()
        if gameInfo.notificationWasDisplayed != true {
        goingTo = 18
            toggleBlur(On: true)
            notificationPopUp()
            
        } else {
            performSegue(withIdentifier: "18", sender: self)
        }
    }
    @IBAction func fifteen(_ sender: Any) {
        fetch()
        if gameInfo.notificationWasDisplayed != true {
        goingTo = 15
            toggleBlur(On: true)
        notificationPopUp()
        } else {
            performSegue(withIdentifier: "15", sender: self)
        }
    }
    @IBAction func seventeen(_ sender: Any) {
        fetch()
        if gameInfo.notificationWasDisplayed != true {
        goingTo = 17
            toggleBlur(On: true)
        notificationPopUp()
        } else {
            performSegue(withIdentifier: "17", sender: self)
        }
    }
    @IBAction func nineteen(_ sender: Any) {
        fetch()
        if gameInfo.notificationWasDisplayed != true {
            
        goingTo = 19
            toggleBlur(On: true)
        notificationPopUp()
        } else {
            performSegue(withIdentifier: "19", sender: self)
        }
    }
    @IBAction func sixteen(_ sender: Any) {
        fetch()
        if gameInfo.notificationWasDisplayed != true {
        goingTo = 16
            toggleBlur(On: true)
        notificationPopUp()
        } else {
            performSegue(withIdentifier: "16", sender: self)
        }
    }
    @IBAction func bull(_ sender: Any) {
        fetch()
        if gameInfo.notificationWasDisplayed != true {
        goingTo = 25
            toggleBlur(On: true)
        notificationPopUp()
        } else {
            performSegue(withIdentifier: "bull", sender: self)
        }
    }
    
    
    
    @IBAction func toView(_ sender: Any) {
        performSegue(withIdentifier: "view", sender: self)
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //    MARK: Go back a player after hitting no hits
    @IBAction func returnPlayer(_ sender: Any) {
        gameInfo.currentPlayer = gameInfo.currentPlayer-1
        
        do {
            try managedContext.save()
        } catch {
            print("Could not save data")
        }
        fetch()
        
        timesHit = timesHit-1
        if timesHit == 0 {
            returnPlayer.isHidden = true
        }
        
        switch gameInfo.numberOfPlayers {
        case 2:
            if gameInfo.currentPlayer == 0 {
                gameInfo.currentPlayer = 2
                
                do {
                    try managedContext.save()
                } catch {
                    print("Could not save data")
                }
                fetch()
            }
        case 3:
            if gameInfo.currentPlayer == 0 {
                gameInfo.currentPlayer = 3
                
                do {
                    try managedContext.save()
                } catch {
                    print("Could not save data")
                }
                fetch()
            }
        case 4:
            if gameInfo.currentPlayer == 0 {
                gameInfo.currentPlayer = 4
                
                do {
                    try managedContext.save()
                } catch {
                    print("Could not save data")
                }
                fetch()
            }
        default:
            break
        }
        
        switch gameInfo.numberOfPlayers {
        case 2:
            
            switch gameInfo.currentPlayer {
            case 1:
                currentPlayerTitle.text = "It is \(p1.name!)'s turn"
                upNextPlayerTitle.text = "\(p2.name!) is up next"
            case 2:
                currentPlayerTitle.text = "It is \(p2.name!)'s turn"
                upNextPlayerTitle.text = "\(p1.name!) is up next"
            default:
                print("Error in player order tracker")
            }
            
        case 3:
            
            switch gameInfo.currentPlayer {
            case 1:
                currentPlayerTitle.text = "It is \(p1.name!)'s turn"
                upNextPlayerTitle.text = "\(p2.name!) is up next"
            case 2:
                currentPlayerTitle.text = "It is \(p2.name!)'s turn"
                upNextPlayerTitle.text = "\(p3.name!) is up next"
            case 3:
                currentPlayerTitle.text = "It is \(p3.name!)'s turn"
                upNextPlayerTitle.text = "\(p1.name!) is up next"
            default:
                print("Error in player order tracker")
            }
            
        case 4:
            
            switch gameInfo.currentPlayer {
            case 1:
                currentPlayerTitle.text = "It is \(p1.name!)'s turn"
                upNextPlayerTitle.text = "\(p2.name!) is up next"
            case 2:
                currentPlayerTitle.text = "It is \(p2.name!)'s turn"
                upNextPlayerTitle.text = "\(p3.name!) is up next"
            case 3:
                currentPlayerTitle.text = "It is \(p3.name!)'s turn"
                upNextPlayerTitle.text = "\(p4.name!) is up next"
            case 4:
                currentPlayerTitle.text = "It is \(p4.name!)'s turn"
                upNextPlayerTitle.text = "\(p1.name!) is up next"
            default:
                print("Error in player order tracker")
            }
            
        default:
            print("Error in number of players tracker")
        }
        
        alert1.isHidden = false
        alert2.isHidden = false
        currentPlayerTitle.textColor = UIColor.yellow
        upNextPlayerTitle.textColor = UIColor.yellow
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.alert1.isHidden = true
            self.alert2.isHidden = true
            self.currentPlayerTitle.textColor = UIColor.white
            self.upNextPlayerTitle.textColor = UIColor.white
        }


        
    }
    
    
    //    MARK: No hits button
    @IBAction func NoHits(_ sender: Any) {
        gameInfo.numberOfNumsSelected = 0
        gameInfo.numberOfNumsHit = 0
        back.isHidden = true
        
        gameInfo.currentPlayer = gameInfo.currentPlayer+1
        returnPlayer.isHidden = false
        
        timesHit = timesHit+1
        
        do {
            try managedContext.save()
        } catch {
            print("Could not save data")
        }
        fetch()
        
refresh()
    
        alert1.isHidden = false
        alert2.isHidden = false
        currentPlayerTitle.textColor = UIColor.yellow
        upNextPlayerTitle.textColor = UIColor.yellow
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.alert1.isHidden = true
            self.alert2.isHidden = true
            self.currentPlayerTitle.textColor = UIColor.white
            self.upNextPlayerTitle.textColor = UIColor.white
        }
    }
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        fetch()
        

        print("Number of numbers hit: \(gameInfo.numberOfNumsHit)")
        print("Number of numbers selected: \(gameInfo.numberOfNumsSelected)")
        //Ad
        self.view.addSubview(adBanner)
        
        
        adBanner.center.x = self.view.center.x
        adBanner.frame.origin.y = self.view.frame.height - adBanner.frame.height
        
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
        
        updateBtn.isEnabled = false
        
        
        p1TempScore.text = nil
        p2TempScore.text = nil
        p3TempScore.text = nil
        p4TempScore.text = nil
        
//        textFieldDelegates()
//        setTextFields()
        
        setUps()
        
        
        switch gameInfo.numberOfPlayers {
        case 2:
            player1Ename.text = p1.name!
            player2Ename.text = p2.name!
            player3Ename.isHidden = true
            player4Ename.isHidden = true
        case 3:
            player1Ename.text = p1.name!
            player2Ename.text = p2.name!
            player3Ename.text = p3.name!
            player4Ename.isHidden = true
        case 4:
            player1Ename.text = p1.name!
            player2Ename.text = p2.name!
            player3Ename.text = p3.name!
            player4Ename.text = p4.name!
        default:
            break
        }
        
        
        secondNumberDirections.isHidden = true
        
        switch gameInfo.currentPlayer {
        case 1:
            currentPlayerName = p1.name!
        case 2:
            currentPlayerName = p2.name!
        case 3:
            currentPlayerName = p3.name!
        case 4:
            currentPlayerName = p4.name!
        default:
            break
        }

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(rawValue: "refresh"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissPop), name: NSNotification.Name(rawValue: "dismissPop"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(hideUndoButton), name: NSNotification.Name(rawValue: "hideUndo"), object: nil)
        
        
        
        noHitsDirections.alpha = 0.0

        extraTwo.alpha = 0
        
        extraThree.alpha = 0
        
        switch gameInfo.numberOfPlayers {
        case 2:
            
            switch gameInfo.currentPlayer {
            case 1:
                currentPlayerTitle.text = "It is \(p1.name!)'s turn"
                upNextPlayerTitle.text = "\(p2.name!) is up next"
            case 2:
                currentPlayerTitle.text = "It is \(p2.name!)'s turn"
                upNextPlayerTitle.text = "\(p1.name!) is up next"
            default:
                print("Error in player order tracker")
            }
            
        case 3:
            
            switch gameInfo.currentPlayer {
            case 1:
                currentPlayerTitle.text = "It is \(p1.name!)'s turn"
                upNextPlayerTitle.text = "\(p2.name!) is up next"
            case 2:
                currentPlayerTitle.text = "It is \(p2.name!)'s turn"
                upNextPlayerTitle.text = "\(p3.name!) is up next"
            case 3:
                currentPlayerTitle.text = "It is \(p3.name!)'s turn"
                upNextPlayerTitle.text = "\(p1.name!) is up next"
            default:
                print("Error in player order tracker")
            }
            
        case 4:
            
            switch gameInfo.currentPlayer {
            case 1:
                currentPlayerTitle.text = "It is \(p1.name!)'s turn"
                upNextPlayerTitle.text = "\(p2.name!) is up next"
            case 2:
                currentPlayerTitle.text = "It is \(p2.name!)'s turn"
                upNextPlayerTitle.text = "\(p3.name!) is up next"
            case 3:
                currentPlayerTitle.text = "It is \(p3.name!)'s turn"
                upNextPlayerTitle.text = "\(p4.name!) is up next"
            case 4:
                currentPlayerTitle.text = "It is \(p4.name!)'s turn"
                upNextPlayerTitle.text = "\(p1.name!) is up next"
            default:
                print("Error in player order tracker")
            }
            
        default:
            print("Error in number of players tracker")
        }
        
        
        returnPlayer.isHidden = true

        alert1.isHidden = true
        alert2.isHidden = true
        back.isHidden = false
        

        back.isEnabled = false
        fetch()
        if rusumingGame == true {
            
        if gameInfo.numberOfNumsSelected != gameInfo.numberOfNumsHit && gameInfo.numberOfNumsSelected != 0 {
            toggleBlur(On: false)
            switch gameInfo.currentPlayer {
            case 1:
                self.currentPlayerName = p1.name!
            case 2:
                self.currentPlayerName = p2.name!
            case 3:
                self.currentPlayerName = p3.name!
            case 4:
                self.currentPlayerName = p4.name!
            default:
                break
            }
            
            secondNumberDirections.isHidden = false
            secondNumberDirections.text = "\(currentPlayerName!), please select your other number or press the No Hits button to skip to the next player"
            
            editSocresBtn.isEnabled = false
            
            extraTwo.alpha = 0
            extraThree.alpha = 0
            notYes.alpha = 1
            notNo.alpha = 1
            notText.text = "Did you hit more than one number that counts in cricket?"
            includedNumbersText.alpha = 0
        }
            
            }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if rusumingGame != true {
            gameInfo.currentLocation = 4
            gameInfo.notificationWasDisplayed = false
            gameInfo.notificationYesWasPressed = false
            saveData()
            fetch()
        }
        
        if rusumingGame == true {
            rusumingGame = false
            switch gameInfo.currentLocation {
            case 6:
                performSegue(withIdentifier: "16", sender: self)
            case 7:
                performSegue(withIdentifier: "20", sender: self)
            case 8:
                performSegue(withIdentifier: "18", sender: self)
            case 9:
                performSegue(withIdentifier: "17", sender: self)
            case 10:
                performSegue(withIdentifier: "15", sender: self)
            case 11:
                performSegue(withIdentifier: "19", sender: self)
            case 12:
                performSegue(withIdentifier: "bull", sender: self)
            case 13:
                performSegue(withIdentifier: "resumeToScores", sender: self)
            case 14:
                performSegue(withIdentifier: "resumeToWinningView", sender: self)
            default:
                break
            }
        }


        notification.layer.cornerRadius = 5
        socresEditView.layer.cornerRadius = 5
        
        print(p2.name!)
        print(p3.name!)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.adBanner.removeFromSuperview()

    }
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "20" {
            let nextVC = segue.destination as! Twenty
            
            nextVC.managedContext = managedContext
        }
        
        if segue.identifier == "17" {
            let nextVC = segue.destination as! seventeen
            
            nextVC.managedContext = managedContext
            
        }
        
        if segue.identifier == "18" {
            let nextVC = segue.destination as! Eightteen
            
            nextVC.managedContext = managedContext
            
        }
        
        if segue.identifier == "15" {
            let nextVC = segue.destination as! Fifteen
            
            nextVC.managedContext = managedContext
        }
        
        if segue.identifier == "19" {
            let nextVC = segue.destination as! Nineteen
            
            nextVC.managedContext = managedContext
            
        }
        
        if segue.identifier == "16" {
            let nextVC = segue.destination as! Sixteen
            
            nextVC.managedContext = managedContext
            
        }
        
        if segue.identifier == "bull" {
            let nextVC = segue.destination as! Bull
            
            nextVC.managedContext = managedContext
        }
        
        if segue.identifier == "view" {
            let popUpScoreboard = segue.destination as! ScoreBoard2
            
            popUpScoreboard.managedContext = managedContext
        }
        if segue.identifier == "resumeToScores" {
            let scoreboard = segue.destination as! ScoreBoard
            scoreboard.hideBackBtn = true
            scoreboard.managedContext = managedContext
        }
    }
 
    
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
