//
//  winner.swift
//  Score Board for Darts
//
//  Created by Austin Senich on 7/30/18.
//  Copyright © 2018 Austin Senich. All rights reserved.
//

import UIKit
import CoreData

class winner: UIViewController {
    
    var overAndOver = true
    
    
    //    MARK: Core Data Properites
    var managedContext: NSManagedObjectContext!
    
    var p1: PlayerInfo!
    var p2: PlayerInfo!
    var p3: PlayerInfo!
    var p4: PlayerInfo!
    
    var gameInfo: CurrentGameInfo!
    
    // MARK: Outlets
    @IBOutlet weak var confeti1: UIImageView!
    @IBOutlet weak var confeti2: UIImageView!
    @IBOutlet weak var winner: UILabel!
    
    

    // MARK: Actions
    @IBAction func viewScoreBoard(_ sender: Any) {

        performSegue(withIdentifier: "seeFinalScores", sender: self)
    }
    
    @IBAction func reset(_ sender: Any) {
        reset()
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetch()
        gameInfo.currentLocation = 14
        saveData()
        fetch()
        
        
        // Do any additional setup after loading the view.
        confeti1.isHidden = false
        confeti2.isHidden = true
        
        winner.text = "\(gameInfo.winningPlayer!) won!"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 3) {
            self.confeti1.center = self.view.center
        }
/*
        repeat {
            confeti1.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.confeti1.isHidden = true
                self.confeti2.isHidden = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.confeti2.isHidden = true
                    self.confeti1.isHidden = false
                }
            }
        } while overAndOver == true
 */
    }
    
    
    func reset() {
//       Clears all player info
        let PlayerInfoFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "PlayerInfo")
        
        let deleteAllPlayerInfo = NSBatchDeleteRequest(fetchRequest: PlayerInfoFetch)
        
        do {
            let PlayerInfoDeleteResults = try managedContext.execute(deleteAllPlayerInfo)
        } catch {
            print("Something went wrong when attempting to delete all player info")
        }
        
//        Clears all game info
        let GameInfoFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CurrentGameInfo")
        
        let deleteAllCurentGameInfo = NSBatchDeleteRequest(fetchRequest: GameInfoFetchRequest)
        
        do {
            let CurrentGameInfoDeleteResults = try managedContext.execute(deleteAllCurentGameInfo
            
            )
        } catch {
            print("There was an error when attempting to delete all Current Game data")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seeFinalScores" {
            let finalScoreboard = segue.destination as! ScoreBoard2
            
            finalScoreboard.managedContext = managedContext
        }
    }
    
    func fetch() {
        let request: NSFetchRequest<PlayerInfo> = PlayerInfo.fetchRequest()
        let results = try! managedContext.fetch(request)
        
        let gameRequest: NSFetchRequest<CurrentGameInfo> = CurrentGameInfo.fetchRequest()
        let gameresults = try! managedContext.fetch(gameRequest)
        
        gameInfo = gameresults.first!
        
        if Int(results.count) > 0 {
            print("Game Not finished")
        } else {
            print("No current game")
        }
        
    }
    func saveData() {
        do {
            try managedContext.save()
        } catch {
            print("Could Not Save Data")
        }
    }

}
